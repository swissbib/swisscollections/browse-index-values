
package utils

import scala.io.Source

object OpenResourceWithLines {
  def apply(path: String): Iterator[String] = {
    Source.fromInputStream(getClass.getResourceAsStream(path)).getLines()
  }
}
