import java.io.ByteArrayOutputStream
import java.util.Optional
import java.util.zip.Inflater

val decompresser = new Inflater
decompresser.setInput(zippedByteStream)
val bos = new ByteArrayOutputStream(zippedByteStream.length)
val buffer = new Array[Byte](8192)
while ( {
  !decompresser.finished
}) {
  val size = decompresser.inflate(buffer)
  bos.write(buffer, 0, size)
}
opt = Optional.of(bos.toString)
decompresser.end()
