/*
 * browse-index-values (swisscollections)
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



package utils


import ch.swisscollections.alphabeticbrowse.utilities.MongoClient

class MongoWrapperTest extends MongoClient{

  import utils.MongoWrapperTest.records

  override def getGndDocument(gndId: String): Option[String] = records.get(gndId)
}


object MongoWrapperTest {

  private val records: Map[String, String] = {
    OpenResourceWithLines("/record/gndrecords.txt").map {
      line =>
        val temp = line.split("####")
        (temp(0), temp(1))
    }.toMap
  }


  def apply () = new MongoWrapperTest

}
