/*
* browse-index-values
  * Copyright (C) 2020  UB Basel
*
* This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

package ch.swisscollections.alphabeticbrowse

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import ch.swisscollections.alphabeticbrowse.stream.functions.PersonValueBuilder.subjectAgentPattern
import ch.swisscollections.alphabeticbrowse.stream.functions.{CallNumberValueBuilder, GndValues, GndValuesProvider, IdFieldValueBuilder, PersonValueBuilder, RemoveNamespaces, SolrDocCreator, SubjectValueBuilder, TitleValueBuilder}
import ch.swisscollections.alphabeticbrowse.utilities.CombineWithGndIdAndVariations.cleanup
import ch.swisscollections.alphabeticbrowse.utilities.{CombineWithGndIdAndVariations, XmlHelper}
import org.scalatest.funsuite.AnyFunSuite
import utils.{GndValuesProviderTest, MongoWrapperTest, OpenResource}

import scala.util.matching.Regex
import scala.xml.XML

class BrowseValuesTest extends AnyFunSuite{


  test ("test addressee terms component - multiple adressees") {
    val persons: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_list_rcp.xml")),GndValuesProviderTest)

    assert(persons.length == 6)

    //println(XmlHelper.makeXmlOneLiner(XML.loadString(adressees)).filter(_ >= ' '))
    //throw away all control characters - includes carriage return new line
    //assert("""<addressees> <aut>Grieder, Hans</aut> <rcp>Meier, Fritz (1912-1998)</rcp> </addressees>""" ==
    //  XmlHelper.makeXmlOneLiner(XML.loadString(adressees)).filter(_ >= ' '))

  }

  test ("test correct formatting of browse values") {
    val pattern = """<field name="browseAddressee">(.*?)</field>""".r
    val longAddressee = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_with_very_long_addressee.xml")),GndValuesProviderTest)

    /*
    assert(longAddressee
      .flatMap(pattern.findAllMatchIn(_))
      .map(_.group(1)).map(_.split("x" * 10)(0))
      .count(_.length <= 50) == 1)
*/
    //first part should be exactly 50 because it is cut off
    assert(longAddressee
      .flatMap(pattern.findAllMatchIn(_))
      .map(_.group(1)).map(_.split("#" * 10)(0))
      .count( firstPart => {
        //println(firstPart.length)
        firstPart.length == 50
        }
      ) == 1)


    //the second part is longer than 50 (complete value delivered by the xslt template)
    assert(longAddressee
      .flatMap(pattern.findAllMatchIn(_))
      .map(_.group(1)).map(_.split("#" * 10)(1))
      .count(_.length > 50) == 1)


  }

  test ("author") {
    val pattern = """<field name="browseAu(.*?)">(.*?)</field>""".r
    val longAddressee: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_list_person.xml")),GndValuesProviderTest)

   val test = longAddressee.filterNot(s => {
      pattern.findAllMatchIn(s).nonEmpty})

    //assert(longAddressee.filterNot(s => {
      //pattern.findAllMatchIn(s.toString).isEmpty
    //})
      //.flatMap(pattern.findAllMatchIn(_)).mkString.contains("""<field name="browseAuthor">klee, paul"""))

    assert(longAddressee
      .mkString.contains("""<field name="browseCreator">klee paul"""))

    //assert(longAddressee
      //.flatMap(pattern.findAllMatchIn(_)).mkString.contains("""<field name="browseAuthor">blabla"""))
  }

  test ("nekrolog") {
    val longAddressee: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_nekrolog.xml")),GndValuesProviderTest)

    assert(longAddressee
      .mkString.contains("""<field name="browseNekrolog">raillard margaretha 1750 1791"""))

    assert(longAddressee
      .mkString.contains("""<field name="browseZurichPerson">heiliger felix 1811 1857"""))

    assert(longAddressee
      .mkString.contains("""<field name="browseSubjectAgent">no bod 1985 """))

    assert(longAddressee
      .mkString.contains("""<field name="browseAgent">merian maria sibylla """))
  }

  test ("contributors based on negative filter") {

    val contributorPattern: Regex = "<(rcp|aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl|cor|col|ann|ins|crr|pfr|arr|pat|ato|scl|bnd|prf|cnd|itr|sng|mus|pbl|prt|bsl|plt|prm|fmk|pht|hnr|mfr|ilu|ill|ctg|cmp|art|ltg|clt|ppm|pra|etr|rsp|fac|scr|nrt|egr|fmo|dnr|sll|dte|dto|ins|com|own|dpt|len|agent|subagent|nekrolog|turPerson)>(.*?)</(rcp|aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl|cor|col|ann|ins|crr|pfr|arr|pat|ato|scl|bnd|prf|cnd|itr|sng|mus|pbl|prt|bsl|plt|prm|fmk|pht|hnr|mfr|ilu|ill|ctg|cmp|art|ltg|clt|ppm|pra|etr|rsp|fac|scr|nrt|egr|fmo|dnr|sll|dte|dto|ins|com|own|dpt|len|agent|subagent|nekrolog|turPerson)>".r

    val xsltResult: String = XmlHelper.personValuesTermsComponent(
      RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_nekrolog.xml"))
    )


    //todo
    // split mit \n ist sehr brüchig
    // besser wäre ein regex der wie folgt sucht
    // <.*?>.*?</.*?>
    // erster match in Spitzklammern entspricht zweitem entsprechendem Endtag
    // das geht, was aber gerade nicht wie...
    val splittedAllFoundValues = xsltResult.split("\n").toList
    val notWantedMatches = contributorPattern.findAllMatchIn(xsltResult).toList.map(_.toString())

    val truePositives = splittedAllFoundValues.filterNot(
      expression => notWantedMatches.contains(expression)
    )

    //val temp = contributorPattern.findAllMatchIn(xsltResult).toList.map(_.toString())
    //val contributor = xsltResult.filterNot(s => contributorPattern.findAllMatchIn(s.toString).nonEmpty).
    //  map(positiveMatches =>
    //      s"""<field name="browseContributor">${AnalyzeWrapper.generalAnalyzer(positiveMatches.toString)}</field>""").toList

    assert(splittedAllFoundValues.length == 19)
    assert(truePositives.length == 3)
    assert(notWantedMatches.length == 16)

    //val contributor: Seq[String] = PersonValueBuilder(v1 = "anykey",
    //  v2 = RemoveNamespaces(v1 = "anykey", v2 = OpenResource("/record/han_record_nekrolog.xml")))

    //assert(contributor
    //  .mkString.contains("""<field name="browseContributor">tester, thea (1466-1838) """))

    //assert(contributor
    //  .mkString.contains("""<field name="browseContributor">gex, regula (1945-) """))

  }

  test ("contributors") {
    val contributor: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_nekrolog.xml")),GndValuesProviderTest)

    assert(contributor
      .mkString.contains("""<field name="browseContributor">tester thea 1466 1838 """))

    assert(contributor
      .mkString.contains("""<field name="browseContributor">gex regula 1945 """))

  }

  test ("non sorting characters") {
    val title: Seq[String] = TitleValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_title.xml")),GndValuesProviderTest)

    val person: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_title.xml")),GndValuesProviderTest)

    assert(title
    .mkString.contains("""<field name="browseTitleBrowse">schwarze spinne """))

    assert(title
      .mkString.contains("""Die schwarze Spinne</field>"""))

    assert(title
      .mkString.contains("""<field name="browseWork">gotthelf jeremias 1797 1854 schwarze spinne"""))

    assert(title
      .mkString.contains("""Gotthelf, Jeremias (1797-1854) / Die schwarze Spinne#"""))

    assert(title
      .mkString.contains("""<field name="browseTitleBrowse">das alt gannyer bad """))

    assert(title
      .mkString.contains("""<field name="browseTitleBrowse">liturgische bucher offizium """))

    assert(person
      .mkString.contains("""<field name="browseAgent">fruhling maja """))

    assert(person
      .mkString.contains("""Frühling, Maja von"""))

    assert(person
      .mkString.contains("""<field name="browseAgent">gesperrt blabla """))

    assert(person
      .mkString.contains("""<field name="browseContributor">otto joh honegger """))

    assert(person
      .mkString.contains("""<field name="browseAgent">otto partner liestal """))

    assert(person
      .mkString.contains("""<field name="browseAgent">saqueboutiers de toulouse """))

    assert(person
      .mkString.contains("""<field name="browsePrinter">hochobrigkeitliche buchdruckerey bern """))

  }

  test ("title") {
    val title: Seq[String] = TitleValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_title.xml")),GndValuesProviderTest)

    //assert(title
      //.mkString.contains("""<field name="browseTitle">schwarze spinne                                   ##########Die schwarze Spinne</field>"""))

  assert(title
    .mkString.contains("""<field name="browseTitleBrowse">predigtniederschriften                            ##########Predigtniederschriften</field>"""))
}

  test ("work title") {
    val title: Seq[String] = TitleValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_work-title.xml")),GndValuesProviderTest)

    assert(title
      .mkString.contains("""##########Quintette, Violine (2), Viola, Violoncello, Klavier. B 28, A-Dur#"""))

    assert(title
      .mkString.contains("""##########Dvořák, Antonín (1841-1904) / Quintette, Violine (2), Viola, Violoncello, Klavier. B 28, A-Dur#"""))

    assert(title
      .mkString.contains("""##########Dvořák, Antonín (1841-1904) / Quintette, Violine (2), Viola, Violoncello, Klavier. B 999, A-Dur</field>"""))

    assert(title
      .mkString.contains("""##########Dvořák, Antonín (1841-1904) / Piano quintets : = Klavierquintette</field>"""))

  }

  test ("subjects, publisher, date") {
    val subject: Seq[String] = SubjectValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_berner-bibliographie.xml")),GndValuesProviderTest)

    assert(subject
      .mkString.contains("""<field name="browseBernClassification">2.14.2.2 ballenberg"""))

    assert(subject
      .mkString.contains("""<field name="browseBernClassification">2.10.8.2 ballenberg"""))

    assert(subject
      .mkString.contains("""<field name="browseBernPlace">ballenberg freilichtmuseum gemusegarten"""))

    assert(subject
      .mkString.contains("""<field name="browseBernTopic">museen freilichtmuseen ballenberg gemusegarten"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">singstimme mehrere orchester 1 tonband 1"""))

    assert(subject
      .mkString.contains("""Singstimme (mehrere), Orchester (1), Tonband (1)</field>"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">flote 3"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">mezzosopran 1 klavier 1"""))

    assert(subject
      .mkString.contains("""Mezzosopran (1), Klavier (1)"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">chor satb 2 orchester 1"""))

    assert(subject
      .mkString.contains("""Chor, SATB (2), Orchester (1)"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">klavier 4 handig 1"""))

    assert(subject
      .mkString.contains("""Klavier, 4-händig (1)"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">singstimme mehrere orchester 1"""))

    assert(subject
      .mkString.contains("""Singstimme (mehrere), Orchester (1)"""))

    assert(subject
      .mkString.contains("""<field name="browseMedium">violine 5 klavier 8 handig 1"""))

    assert(subject
      .mkString.contains("""Violine (5), Klavier, 8-händig (1)"""))

    assert(subject
      .mkString.contains("""<field name="browseGeo">ballenberg freilichtmuseum gemusegarten"""))

    assert(subject
      .mkString.contains("""<field name="browseGeo">schweiz europa"""))

    assert(subject
      .mkString.contains("""<field name="browseZurichClassification">tur32 """))

    assert(subject
      .mkString.contains("""<field name="browseZurichGeo">schweiz europa """))

    assert(subject
      .mkString.contains("""<field name="browsePlace">aarau """))

    assert(subject
      .mkString.contains("""<field name="browsePlace">riehen """))

    assert(subject
      .mkString.contains("""<field name="browseDate">1963.12.03"""))

    assert(subject
      .mkString.contains("""<field name="browseTopicBrowse">gemuse """))

    assert(subject
      .mkString.contains("""<field name="browseBaselPersonPlace">merian maria sibylla"""))

    assert(subject
      .mkString.contains("""<field name="browseBaselPersonPlace">basel"""))

    assert(subject
      .mkString.contains("""<field name="browseGeo">basel"""))

    assert(subject
      .mkString.contains("""<field name="browseFormImage">bild"""))

    assert(subject
      .mkString.contains("""<field name="browseFormImage">holzstich """))

    assert(subject.mkString.contains("<field name=\"browseSolothurnAgent\">rudt ineichen maria 1879 1980 "))

    assert(subject.mkString.contains("<field name=\"browseSolothurnGeo\">solothurn "))

    val subject2: Seq[String] = SubjectValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_music-genre.xml")),GndValuesProviderTest)

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">lied schweiz """))

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">lied 19 jh """))

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">19 jh schweiz unterrichtswerk """))

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">walzer """))

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">frankreich """))

    assert(subject2
      .mkString.contains("""<field name="browseMusicGenre">bearbeitung """))

  }

  test("initium") {
    val subject: Seq[String] = SubjectValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_initium.xml")), GndValuesProviderTest)

    assert(subject
      .mkString.contains("""<field name="browseInitium">ad commendationem status virginalis"""))

    assert(subject.mkString.contains("""<field name="browseInitium">angelus domini apparuit"""))
  }


  test ("call numbers sort") {
    val callNumber: Seq[String] = CallNumberValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_callNumber.xml")), GndValuesProviderTest)

    assert(callNumber
      .mkString.contains("""<field name="browseFormerCallNumber">c iv 0000022   alt """))

    assert(callNumber
      .mkString.contains("""<field name="browseA100CallNumber">ubh c vib 0000008 """))

    assert(callNumber
      .mkString.contains("""<field name="browseAllCallNumber">ubh c vib 0000008 """))

    assert(callNumber
      .mkString.contains("""<field name="browseAllCallNumber">aa 0000747  0000001 """))

    assert(callNumber
      .mkString.contains("""<field name="browseAllCallNumber">swa pa 0000587 f 0000001  0000001 """))

    assert(callNumber
      .mkString.contains("""UBH C VIb 8</field>"""))

    assert(callNumber
      .mkString.contains("""<field name="browseAllCallNumber">ubh aleph a i 0000010  0000001 """))

    assert(callNumber
      .mkString.contains("""UBH Aleph A I 10:1</field>"""))

  }

  test("gnd enrichment") {
    val person: Seq[String] = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_gnd-enrich.xml")),GndValuesProviderTest)
    //println(person)

    val title: Seq[String] = TitleValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_gnd-enrich.xml")),GndValuesProviderTest)
    val subject: Seq[String] = SubjectValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_gnd-enrich.xml")),GndValuesProviderTest)


    assert(person.mkString.contains("""<field name="browseSubjectAgent">mozart anna maria 1720 1778"""))
    assert(person.mkString.contains("""Mozart, Anna Maria (1720-1778)##########(DE-588)119285428"""))
    assert(person.mkString.contains("""<field name="browseSubjectAgent">pertl maria anna 1720 1778"""))
    assert(person.mkString.contains("""Pertl, Maria Anna (1720-1778)##########(DE-588)119285428##########Mozart, Anna Maria (1720-1778)"""))

    assert(person.mkString.contains("""<field name="browseComposer">dvorac anton 1841 1904"""))
    assert(person.mkString.contains("""Dvořác, Anton (1841-1904)##########(DE-588)11852836X##########Dvořák, Antonín (1841-1904)"""))
    assert(person.mkString.contains("""<field name="browseComposer">dvorak antonin 1841 1904"""))
    assert(person.mkString.contains("""Dvořák, Antonín (1841-1904)##########(DE-588)11852836X"""))

    assert(person.mkString.contains("""Museo svizzero all'aperto Ballenberg (2014-)##########(DE-588)109911599X##########Ballenberg, Freilichtmuseum der Schweiz"""))

    assert(person.mkString.contains("""Expo.02##########(DE-588)2161528-7##########Schweizerische Landesausstellung"""))

    assert(person.mkString.contains("""speyr adrienne 1902 1967                          ##########Speyr, Adrienne von (1902-1967)##########(DE-588)118833545</field>"""))
    assert(person.mkString.contains("""<field name="browseSubjectAgent">speyr adrianne kaegi 1902 1967"""))
    assert(person.mkString.contains("""##########Speyr, Adrianne Kaegi von (1902-1967)##########(DE-588)118833545##########Speyr, Adrienne von (1902-1967)</field>"""))

    assert(title.mkString.contains("""<field name="browseTitleBrowse">edda"""))
    assert(title.mkString.contains("""<field name="browseTitleBrowse">eldre edda islandsk"""))
    assert(title.mkString.contains("""Eldre Edda Islandsk##########(DE-588)4138527-5##########Edda"""))

    assert(title.mkString.contains("""<field name="browseWork">dvorak antonin 1841 1904 klavierquintett op 81"""))
    assert(title.mkString.contains("""Dvořák, Antonín (1841-1904) / Klavierquintett op. 81##########(DE-588)30004786X##########Dvořák, Antonín (1841-1904) / Quintette, Violine (2), Viola, Violoncello, Klavier. B 155, A-Dur<"""))

    assert(subject.mkString.contains("""Gemüsepflanzen##########(DE-588)4020069-3##########Gemüse"""))
    assert(subject.mkString.contains("""<field name="browseTopicBrowse">gemusepflanzen"""))

    assert(!subject.mkString.contains("""Confederazione Helvetica##########(DE-588)4053881-3##########Schweiz, Europa"""))

  }

  test("zbcollections") {
      val person: Seq[String] = PersonValueBuilder(key = "anykey",
        data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/zbc_record_gnd.xml")), GndValuesProviderTest)

    assert(!person.mkString.contains("""Mozart, Wolfgang Amadeus##########(DE-588)118584596"""))
    assert(person.mkString.contains("""Mozart, Wolfgang Amadeus (1756-1791)##########(DE-588)118584596"""))
    assert(person.mkString.contains("""Mozart, Wolffgango (1756-1791)##########(DE-588)118584596"""))
    assert(person.mkString.contains("""<field name="browseCreator">mozart wolfgang amadeus 1756 1791"""))

  }

  ignore ("test correct padding of first part") {

    val pattern = """<field name="browseAddressee">(.*?)</field>""".r
    val shortAddressee = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_with_very_short_addressee.xml")),GndValuesProviderTest)


    val expectedFirst= "Günter - too short .. (1959)                      " //padded with whitespace
    val expectedSecond = "gunter - too short .. (1959)"  //used for presentation purposes

    val splittedTerms =
      shortAddressee
      .flatMap(pattern.findAllMatchIn(_))
      .map(_.group(1)).head.split("x" * 10)

    assert(splittedTerms.head == expectedFirst)
    assert(splittedTerms.tail.head == expectedSecond)

  }

  test("id generation") {

    val id =  IdFieldValueBuilder("(EXLNZ-41SLSP_NETWORK)991125085599705501")
    assert (id == """<field name="id">991125085599705501</field>""")

    val id2 = IdFieldValueBuilder("(ZBC)12345")
    assert(id2 == """<field name="id">(ZBC)12345</field>""")

  }

  ignore ("composition of single fields into solr doc") {

    val addressees = PersonValueBuilder(key = "anykey",
      data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_browse_list_rcp.xml")),GndValuesProviderTest)

    val docWithFields = IdFieldValueBuilder("testkey")  +:
      addressees ::: Nil

    val completeDoc = SolrDocCreator(docWithFields)

    assert(completeDoc == """<doc><field name="id">testkey</field><field name="browseAddressee">Meier, Fritz (1912-1998)                          ##########meier, fritz (1912-1998)</field><field name="browseAddressee">Hipler, Günter - listentest (1959-)               ##########hipler, gunter - listentest (1959-)</field></doc>""")

  }

  /*
  test ("validate Gnd Values Provider for testing") {

    assert( GndValuesProviderTest.getGndValues("(DE-588)109911599X").isDefined )
    assert( GndValuesProviderTest.getGndValues("not valid GND number should be empty").isEmpty )


    assert ((GndValuesProviderTest.getGndValues("(DE-588)109911599X") match {
      case Some(gndValue) =>
        gndValue.gndId
      case _ => ""
    }) == "(DE-588)109911599X")

    assert (GndValuesProviderTest.getGndValues("(DE-588)109911599X") match {
      case Some(gndValue) =>
        gndValue.marcDoc
      case _ => ""
    }) == """<record xmlns="http://www.loc.gov/MARC21/slim" type="Authority"><leader>00000nz  a2200000nc 4500</leader><controlfield tag="001">109911599X</controlfield><controlfield tag="003">DE-101</controlfield><controlfield tag="005">20191128053445.0</controlfield><controlfield tag="008">160502n||azznnaabn           | ana    |c</controlfield><datafield tag="024" ind1="7" ind2=" "><subfield code="a">109911599X</subfield><subfield code="0">http://d-nb.info/gnd/109911599X</subfield><subfield code="2">gnd</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(DE-101)109911599X</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(DE-588)109911599X</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">CH-001821-6</subfield><subfield code="c">CH-001821-6</subfield><subfield code="9">r:CH-001821-6</subfield><subfield code="b">ger</subfield><subfield code="d">9999</subfield><subfield code="e">rda</subfield></datafield><datafield tag="042" ind1=" " ind2=" "><subfield code="a">gnd1</subfield></datafield><datafield tag="043" ind1=" " ind2=" "><subfield code="c">XA-CH-BE</subfield></datafield><datafield tag="065" ind1=" " ind2=" "><subfield code="a">6.8</subfield><subfield code="2">sswd</subfield></datafield><datafield tag="075" ind1=" " ind2=" "><subfield code="b">b</subfield><subfield code="2">gndgen</subfield></datafield><datafield tag="075" ind1=" " ind2=" "><subfield code="b">kiz</subfield><subfield code="2">gndspec</subfield></datafield><datafield tag="079" ind1=" " ind2=" "><subfield code="a">g</subfield><subfield code="q">f</subfield><subfield code="q">s</subfield><subfield code="u">z</subfield><subfield code="u">v</subfield><subfield code="u">w</subfield></datafield><datafield tag="110" ind1="2" ind2=" "><subfield code="a">Ballenberg, Freilichtmuseum der Schweiz</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Freilichtmuseum Ballenberg</subfield><subfield code="g">2014-</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="9">L:fre</subfield><subfield code="a">Ballenberg, Musée suisse en plein air</subfield><subfield code="5">CH-GND</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Musée suisse en plein air Ballenberg</subfield><subfield code="g">2014-</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="9">L:ita</subfield><subfield code="a">Ballenberg, Museo svizzero all'aperto</subfield><subfield code="5">CH-GND</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Museo svizzero all'aperto Ballenberg</subfield><subfield code="g">2014-</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="9">L:eng</subfield><subfield code="a">Ballenberg, Swiss Open-Air Museum</subfield><subfield code="5">CH-GND</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Swiss Open-Air Museum Ballenberg</subfield><subfield code="g">2014-</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Swiss Open Air Museum Ballenberg</subfield><subfield code="g">2014-</subfield></datafield><datafield tag="410" ind1="2" ind2=" "><subfield code="a">Schweizerisches Freilichtmuseum Ballenberg</subfield><subfield code="g">2014-</subfield><subfield code="9">v:Vorlage von 2014</subfield></datafield><datafield tag="510" ind1="2" ind2=" "><subfield code="0">(DE-101)964430657</subfield><subfield code="0">(DE-588)10038219-8</subfield><subfield code="0">https://d-nb.info/gnd/10038219-8</subfield><subfield code="a">Schweizerisches Freilichtmuseum Ballenberg für Ländliche Kultur</subfield><subfield code="4">vorg</subfield><subfield code="4">https://d-nb.info/standards/elementset/gnd#precedingCorporateBody</subfield><subfield code="w">r</subfield><subfield code="i">Vorgaenger</subfield><subfield code="e">Vorgaenger</subfield></datafield><datafield tag="548" ind1=" " ind2=" "><subfield code="a">2014-</subfield><subfield code="4">datb</subfield><subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfEstablishment</subfield><subfield code="w">r</subfield><subfield code="i">Zeitraum</subfield></datafield><datafield tag="550" ind1=" " ind2=" "><subfield code="0">(DE-101)041324862</subfield><subfield code="0">(DE-588)4132486-9</subfield><subfield code="0">https://d-nb.info/gnd/4132486-9</subfield><subfield code="a">Freilichtmuseum</subfield><subfield code="4">obin</subfield><subfield code="4">https://d-nb.info/standards/elementset/gnd#broaderTermInstantial</subfield><subfield code="w">r</subfield><subfield code="i">Oberbegriff instantiell</subfield></datafield><datafield tag="551" ind1=" " ind2=" "><subfield code="0">(DE-101)988689596</subfield><subfield code="0">(DE-588)7611495-8</subfield><subfield code="0">https://d-nb.info/gnd/7611495-8</subfield><subfield code="a">Hofstetten bei Brienz</subfield><subfield code="4">orta</subfield><subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfBusiness</subfield><subfield code="w">r</subfield><subfield code="i">Ort</subfield></datafield><datafield tag="551" ind1=" " ind2=" "><subfield code="0">(DE-101)040057658</subfield><subfield code="0">(DE-588)4005765-3</subfield><subfield code="0">https://d-nb.info/gnd/4005765-3</subfield><subfield code="a">Kanton Bern</subfield><subfield code="4">geow</subfield><subfield code="4">https://d-nb.info/standards/elementset/gnd#spatialAreaOfActivity</subfield><subfield code="w">r</subfield><subfield code="i">Wirkungsraum</subfield></datafield><datafield tag="670" ind1=" " ind2=" "><subfield code="a">Homepage</subfield><subfield code="b">Stand: 02.05.2016</subfield><subfield code="u">http://www.ballenberg.ch</subfield></datafield></record>""")

  }
*/
  test ("variant values ") {

    val variants = GndValuesProviderTest.getGndValues("(DE-588)4020069-3").get.variantsValues()
    val expected = List("Gemüsepflanzen")

    assert (variants == expected)

    val variants1 = GndValuesProviderTest.getGndValues("(DE-588)11852836X").get.variantsValues()

    val expected1 = List(
      "Dvọřák, Antonín (1841-1904)",
      "Dvořak, Anton (1841-1904)",
      "Dvorak, Anton (1841-1904)",
      "Dvořác, Anton (1841-1904)",
      "Dvořak, Antoine (1841-1904)",
      "Dvořak, Antonin H. (1841-1904)",
      "Dvorák, Antonín H. (1841-1904)",
      "Dvorák, Antonin (1841-1904)",
      "Dvořák, Antonín Leopold (1841-1904)",
      "Dvořák, Antonín L. (1841-1904)",
      "Dvořák, ... (1841-1904)",
      "Dvořák-Kreisler, ... (1841-1904)",
      "Dvoržak, Antonin (1841-1904)",
      "Dvořák, A. (1841-1904)",
      "Dvořák, Antonin (1841-1904)",
      "Dvorak, Antonin (1841-1904)",
      "Dvořák, Anton (1841-1904)",
      "Dvorzak, Anton (1841-1904)",
      "Dvorzhak, Antonin (1841-1904)"
    )
    assert(variants1 == expected1)


    val variants2 = GndValuesProviderTest.getGndValues("(DE-588)2161528-7").get.variantsValues()
    val expected2 = List(
      "Landesausstellung 6, Biel u.a. (2002)",
      "Expo 6, Biel u.a. (2002)",
      "Expo.01 6, Biel u.a. (2002)",
      "Expo.02 6, Biel u.a. (2002)",
      "Exposition Nationale Suisse 6, Biel u.a. (2002)",
      "Esposizione Nazionale Svizzera 6, Biel u.a. (2002)",
      "Exposizum Naziunala Svizra 6, Biel u.a. (2002)",
      "Swiss National Exhibition 6, Biel u.a. (2002)",
      "Schweizerische Landesausstellung 2002",
      "Expo, Biel Bern (2002)",
      "Expo, Murten (2002)",
      "Expo, Neuenburg Schweiz (2002)",
      "Expo, Yverdon-les-Bains (2002)",
      "Expo.02",
      "Expo (2002)"
    )
    assert(variants2 == expected2)


  }

  ignore ("neuer Test unter Silvias Beobachtung") {
    val ressource = OpenResource("/record/han_record_gnd-enrich.xml")
    val results = PersonValueBuilder("key", ressource, GndValuesProviderTest)
    //println(results)

  }

  ignore ("test to analyze the behaviour of the mongo wrapper") {
    val data = RemoveNamespaces(key = "anykey", data = OpenResource("/record/han_record_gnd-enrich.xml"))
    val persons: String = XmlHelper.personValuesTermsComponent(data)

    //println(persons)

    val agents = subjectAgentPattern.findAllMatchIn(persons).flatMap(regexMatch =>
      CombineWithGndIdAndVariations(
        bibValue =  regexMatch.group(1),
        endPoint = "browseSubjectAgent",
        gndValuesProvider=  GndValuesProvider,
        rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
          _,
          analyzerName = "patternReplace"),
        cleanUpPresentationValueFunction = cleanup)).toList

    //println(agents)

  }


}

