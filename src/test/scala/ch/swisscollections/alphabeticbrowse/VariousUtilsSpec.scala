/*
* browse-index-values
  * Copyright (C) 2020  UB Basel
*
* This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/


package ch.swisscollections.alphabeticbrowse

import ch.swisscollections.alphabeticbrowse.utilities.MongoWrapper
import org.scalatest.funsuite.AnyFunSuite

import scala.util.matching.Regex

class VariousUtilsSpec extends AnyFunSuite {

  test ("correct regex for search in MongoDB") {
    assert(MongoWrapper.gndIdRegex.findFirstMatchIn("(DE-588)118702742").map(_.group(1)).get == "118702742")
  }

}
