/*
 *
 *  * swisscollections - browse-index-values
 *  * Copyright (C) 2021  UB Basel
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU Affero General Public License as
 *  * published by the Free Software Foundation, either version 3 of the
 *  * License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU Affero General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU Affero General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *
 *
 */

package ch.swisscollections.alphabeticbrowse

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import org.scalatest.funsuite.AnyFunSuite

class AnalyzerSpec extends AnyFunSuite {

  ignore("test analyzer: remove articles with angled brackets for sorting") {

    val test = "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions"
    val result = AnalyzeWrapper.generalAnalyzer(test,"patternReplace").split(s"${"x"*10}")
    assert(result(0) == "my (gunter) franzosisch:  fenetre book  alphabet o")
    assert(result(1) == "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions")
  }

  ignore("test analyzer: basic analyzer doesn't remove angled brackets for sorting") {

    val test = "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions"
    val result = AnalyzeWrapper.generalAnalyzer(test).split(s"${"x"*10}")

    assert(result(0) == "my (gunter) franzosisch:  fenetre book <the> alpha")
    assert(result(1) == "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions")

  }

  ignore("test analyzer: reference to not existing analyzer") {

    val test = "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions"
    val result = AnalyzeWrapper.generalAnalyzer(test, analyzerName = "not existing analyzer").
      split(s"${"x"*10}")

    assert(result(0) == "my (gunter) franzosisch:  fenetre book <the> alpha")
    assert(result(1) == "my (Günter) Französisch:  Fenêtre Book <the> alphabet of Emotions")

  }



}

