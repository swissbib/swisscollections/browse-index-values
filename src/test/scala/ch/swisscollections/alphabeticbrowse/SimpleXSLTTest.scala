/*
 * filter
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.alphabeticbrowse


import ch.swisscollections.alphabeticbrowse.utilities.XmlHelper
import org.scalatest.funsuite.AnyFunSuite
import utils.MongoWrapperTest

import scala.xml.XML


class SimpleXSLTTest extends AnyFunSuite {

  val sR = """<marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd"><leader>00000nam a2200000uc 4500</leader><controlfield tag="001">99537220105504</controlfield><controlfield tag="005">20200119195152.0</controlfield><controlfield tag="008">840509s1979    gw       m    00| | ger d</controlfield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(swissbib)083003428-41slsp_network</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">083003428</subfield><subfield code="9">ExL</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(IDSBB)000053722DSV01</subfield></datafield><datafield tag="035" ind1=" " ind2=" "><subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield></datafield><datafield tag="040" ind1=" " ind2=" "><subfield code="a">SzZuIDS BS/BE</subfield><subfield code="b">ger</subfield><subfield code="e">kids</subfield><subfield code="d">CH-ZuSLS</subfield></datafield><datafield tag="072" ind1=" " ind2="7"><subfield code="a">mg</subfield><subfield code="2">SzZuIDS BS/BE</subfield></datafield><datafield tag="082" ind1="1" ind2="4"><subfield code="a">610</subfield><subfield code="2">15</subfield></datafield><datafield tag="100" ind1="1" ind2=" "><subfield code="a">Hentschel, Ralph-Christian</subfield></datafield><datafield tag="245" ind1="1" ind2="0"><subfield code="a">Franz Schönenberger (1865-1933)</subfield><subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield><subfield code="c">von Ralph-Christian Hentschel</subfield></datafield><datafield tag="264" ind1=" " ind2="1"><subfield code="a">München</subfield><subfield code="c">1979</subfield></datafield><datafield tag="300" ind1=" " ind2=" "><subfield code="a">186 S., Portr.</subfield><subfield code="c">22 cm</subfield></datafield><datafield tag="336" ind1=" " ind2=" "><subfield code="b">txt</subfield><subfield code="2">rdacontent</subfield></datafield><datafield tag="337" ind1=" " ind2=" "><subfield code="b">n</subfield><subfield code="2">rdamedia</subfield></datafield><datafield tag="338" ind1=" " ind2=" "><subfield code="b">nc</subfield><subfield code="2">rdacarrier</subfield></datafield><datafield tag="502" ind1=" " ind2=" "><subfield code="a">Diss. med. München</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="520" ind1=" " ind2=" "><subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield><subfield code="5">ube-B583</subfield></datafield><datafield tag="600" ind1="1" ind2="7"><subfield code="a">Schönenberger, Franz</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="650" ind1=" " ind2="7"><subfield code="a">Medizin</subfield><subfield code="z">Deutschland</subfield><subfield code="y">Geschichte 19. Jh</subfield><subfield code="2">idsbb</subfield></datafield><datafield tag="655" ind1=" " ind2="7"><subfield code="a">Hochschulschrift</subfield><subfield code="2">gnd-content</subfield></datafield><datafield tag="852" ind1="4" ind2=" "><subfield code="b">A100</subfield><subfield code="c">MAG</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="8">2245498210005504</subfield></datafield><datafield tag="949" ind1=" " ind2=" "><subfield code="3">2245498210005504</subfield><subfield code="o">BOOK</subfield><subfield code="x">Akz: 81 T</subfield><subfield code="2">A1001274783</subfield><subfield code="c">MAG</subfield><subfield code="1">2345498200005504</subfield><subfield code="p">01</subfield><subfield code="j">UBH Diss 1981,172</subfield><subfield code="b">A100</subfield></datafield></marc:record>"""

  val oneLiner = """<record> <leader>00000nam a2200000uc 4500</leader> <controlfield tag="001">99537220105504</controlfield> <controlfield tag="005">20200119195152.0</controlfield> <controlfield tag="008">840509s1979 gw m 00| | ger d</controlfield> <datafield ind2=" " ind1=" " tag="035"> <subfield code="a">(swissbib)083003428-41slsp_network</subfield> </datafield> <datafield ind2=" " ind1=" " tag="035"> <subfield code="a">083003428</subfield> <subfield code="9">ExL</subfield> </datafield> <datafield ind2=" " ind1=" " tag="035"> <subfield code="a">(IDSBB)000053722DSV01</subfield> </datafield> <datafield ind2=" " ind1=" " tag="035"> <subfield code="a">(EXLNZ-41SLSP_NETWORK)991029235339705501</subfield> </datafield> <datafield ind2=" " ind1=" " tag="040"> <subfield code="a">SzZuIDS BS/BE</subfield> <subfield code="b">ger</subfield> <subfield code="e">kids</subfield> <subfield code="d">CH-ZuSLS</subfield> </datafield> <datafield ind2="7" ind1=" " tag="072"> <subfield code="a">mg</subfield> <subfield code="2">SzZuIDS BS/BE</subfield> </datafield> <datafield ind2="4" ind1="1" tag="082"> <subfield code="a">610</subfield> <subfield code="2">15</subfield> </datafield> <datafield ind2=" " ind1="1" tag="100"> <subfield code="a">Hentschel, Ralph-Christian</subfield> </datafield> <datafield ind2="0" ind1="1" tag="245"> <subfield code="a">Franz Schönenberger (1865-1933)</subfield> <subfield code="b">Biobibliographie eines ärztlichen Vertreters der Naturheilkunde</subfield> <subfield code="c">von Ralph-Christian Hentschel</subfield> </datafield> <datafield ind2="1" ind1=" " tag="264"> <subfield code="a">München</subfield> <subfield code="c">1979</subfield> </datafield> <datafield ind2=" " ind1=" " tag="300"> <subfield code="a">186 S., Portr.</subfield> <subfield code="c">22 cm</subfield> </datafield> <datafield ind2=" " ind1=" " tag="336"> <subfield code="b">txt</subfield> <subfield code="2">rdacontent</subfield> </datafield> <datafield ind2=" " ind1=" " tag="337"> <subfield code="b">n</subfield> <subfield code="2">rdamedia</subfield> </datafield> <datafield ind2=" " ind1=" " tag="338"> <subfield code="b">nc</subfield> <subfield code="2">rdacarrier</subfield> </datafield> <datafield ind2=" " ind1=" " tag="502"> <subfield code="a">Diss. med. München</subfield> </datafield> <datafield ind2=" " ind1=" " tag="520"> <subfield code="a">Schönenberger, Franz</subfield> <subfield code="5">ube-B583</subfield> </datafield> <datafield ind2=" " ind1=" " tag="520"> <subfield code="a">Naturheilkunde / Schönenberger, Franz</subfield> <subfield code="5">ube-B583</subfield> </datafield> <datafield ind2="7" ind1="1" tag="600"> <subfield code="a">Schönenberger, Franz</subfield> <subfield code="2">idsbb</subfield> </datafield> <datafield ind2="7" ind1=" " tag="650"> <subfield code="a">Medizin</subfield> <subfield code="z">Deutschland</subfield> <subfield code="y">Geschichte 19. Jh</subfield> <subfield code="2">idsbb</subfield> </datafield> <datafield ind2="7" ind1=" " tag="655"> <subfield code="a">Hochschulschrift</subfield> <subfield code="2">gnd-content</subfield> </datafield> <datafield ind2=" " ind1="4" tag="852"> <subfield code="b">A100</subfield> <subfield code="c">MAG</subfield> <subfield code="j">UBH Diss 1981,172</subfield> <subfield code="8">2245498210005504</subfield> </datafield> <datafield ind2=" " ind1=" " tag="949"> <subfield code="3">2245498210005504</subfield> <subfield code="o">BOOK</subfield> <subfield code="x">Akz: 81 T</subfield> <subfield code="2">A1001274783</subfield> <subfield code="c">MAG</subfield> <subfield code="1">2345498200005504</subfield> <subfield code="p">01</subfield> <subfield code="j">UBH Diss 1981,172</subfield> <subfield code="b">A100</subfield> </datafield> </record>"""

  def removeNS(record: String): String = XmlHelper.transformNoNS(record)

  ignore("test remove NS transformation") {
    val noNs = removeNS(sR)
    println(XmlHelper.makeXmlOneLiner(XML.loadString(noNs)))
    //assert(noNs.trim.compareToIgnoreCase(oneLiner.trim) == 0)
  }


  ignore ("get id from marcrecord") {

    val noNs = removeNS(sR)
    assert((XML.loadString(noNs) \\ "controlfield")
      .find(cf => (cf \@ "tag") == "001")
      .get
      .text ==
      "99537220105504")

  }

  ignore("build first browse values") {
    val noNs = removeNS(sR)

    val browseValues = XmlHelper.transformIntoBrowseValues(noNs)

    println(XmlHelper.makeXmlOneLiner(XML.loadString(browseValues)))
    //assert(noNs.trim.compareToIgnoreCase(oneLiner.trim) == 0)
  }

  test ("test gnd") {

    println(MongoWrapperTest().getGndDocument("(DE-588a)100000193"))

  }




}
