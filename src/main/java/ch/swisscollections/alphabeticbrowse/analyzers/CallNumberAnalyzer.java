package ch.swisscollections.alphabeticbrowse.analyzers;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordTokenizer;
import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.icu.ICUFoldingFilterFactory;
import org.apache.lucene.analysis.miscellaneous.TrimFilter;
import org.apache.lucene.analysis.miscellaneous.TrimFilterFactory;
import org.apache.lucene.analysis.pattern.PatternReplaceFilterFactory;

import java.util.HashMap;

public class CallNumberAnalyzer extends Analyzer {
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {

        HashMap<String,String> foldingFilterMap = new HashMap<>();

        KeywordTokenizerFactory kwtf =  new KeywordTokenizerFactory(new HashMap<>());
        KeywordTokenizer kwt = (KeywordTokenizer) kwtf.create();

        ICUFoldingFilterFactory icuFilter = new ICUFoldingFilterFactory(foldingFilterMap);

        LowerCaseFilterFactory lcff = new LowerCaseFilterFactory(new HashMap<>());

        HashMap<String, String> prffHM = new HashMap<>();
        prffHM.put("pattern"," :|\\/|, ");
        prffHM.put("replacement","  ");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory replaceSeparatorsWithBlanks = new PatternReplaceFilterFactory(prffHM);

        prffHM = new HashMap<>();
        prffHM.put("pattern",":|\\/|\\.|_|-|\\(|\\)|,|&gt;|'|=");
        prffHM.put("replacement","  ");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory replaceSeparators = new PatternReplaceFilterFactory(prffHM);

        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{1})( |[a-z]|$)");
        prffHM.put("replacement"," 000000$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory oneNumber = new PatternReplaceFilterFactory(prffHM);

        //replaces separating characters with blanks
        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{2})( |[a-z]|$)");
        prffHM.put("replacement"," 00000$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory twoNumbers = new PatternReplaceFilterFactory(prffHM);

        //replaces separating characters with blanks
        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{3})( |[a-z]|$)");
        prffHM.put("replacement"," 0000$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory threeNumbers = new PatternReplaceFilterFactory(prffHM);

        //replaces separating characters with blanks
        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{4})( |[a-z]|$)");
        prffHM.put("replacement"," 000$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory fourNumbers = new PatternReplaceFilterFactory(prffHM);

        //replaces separating characters with blanks
        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{5})( |[a-z]|$)");
        prffHM.put("replacement"," 00$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory fiveNumbers = new PatternReplaceFilterFactory(prffHM);

        //replaces separating characters with blanks
        prffHM = new HashMap<>();
        prffHM.put("pattern"," ([0-9]{6})( |[a-z]|$)");
        prffHM.put("replacement"," 0$1$2");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory sixNumbers = new PatternReplaceFilterFactory(prffHM);


        TrimFilterFactory tfff = new TrimFilterFactory(new HashMap<>());
        TrimFilter tf =  tfff.create(sixNumbers.create(fiveNumbers.create(fourNumbers.create(threeNumbers.create(twoNumbers.create(oneNumber.create(replaceSeparators.create(replaceSeparatorsWithBlanks.create(lcff.create(icuFilter.create(tfff.create(kwt))))))))))));

        return new TokenStreamComponents(kwt, tf);
    }
}