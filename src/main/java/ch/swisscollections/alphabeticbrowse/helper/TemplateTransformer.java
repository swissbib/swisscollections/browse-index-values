/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.helper;


import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;


public class TemplateTransformer {

    private final StringReader record;

    public TemplateTransformer(String recordToTransform) {
        record = new StringReader(recordToTransform);
    }


    public String transform(Transformer transformer) {

        StringWriter transformedRecord = getTransformation(transformer);
        return transformedRecord.toString();

    }

    StringWriter getTransformation(Transformer transformer) {
        Source source = new StreamSource(record);
        StringWriter transformedRecord = new StringWriter();
        StreamResult target = new StreamResult(transformedRecord);

        try {
            transformer.transform(source, target);
        } catch (TransformerException tex) {
            throw new RuntimeException(tex);
        }

        return transformedRecord;

    }



}
