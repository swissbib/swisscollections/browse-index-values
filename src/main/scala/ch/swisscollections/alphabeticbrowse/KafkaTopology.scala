/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.alphabeticbrowse

import ch.swisscollections.alphabeticbrowse.stream.functions.{BrowseValuesFunctionWrapper, GndValuesProvider, RemoveNamespaces, SolrDocCreator}
import ch.swisscollections.alphabeticbrowse.utilities.MongoWrapper
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging


class KafkaTopology extends Logging {

  import Serdes._

  def build(): Topology = {
    val builder = new StreamsBuilder

    val source = builder.stream[String, String](SettingsFromFile.getKafkaInputTopic)

    source
      //only send records from Alma and CMI to the browse index
      //ignore the other sources (bildungsgeschichte)
      .filter((key, _) => isFromAlmaOrCMI(key))
      //get rid of the damn namespaces and other information around the single marc record
      .mapValues(RemoveNamespaces)
      //wraps all the functions used for browse values creation
      .mapValues((key, data) => BrowseValuesFunctionWrapper(key, data, GndValuesProvider))
      //glue all individual field elements together to form a Solrdoc
      .mapValues(SolrDocCreator)
      //replace this with reporting mechanism used in memobase
      .filter(
        //in case there happens a failure in transformation the document isn't sent into the topic
        //we have to decide if it would be better to create a second branch in the DAG for these
        //kind of exceptions and put the record into it - for later
        (key,transformedDoc) =>
          if (transformedDoc.isEmpty) {
            logger.info(s"""empty doc with key: $key - will be filtered out""")
            false
          } else {
            true
          }
          //transformedDoc.nonEmpty
        )
      //filter is needed to avoid messages larger than the default message
      // length of Kafka todo: adjust this on the Kafka cluster
      .filter((key,v) =>

        if (v.getBytes.length > 990000) {
          logger.info(s"""doc longer as 990000 bytes: $key - will be filtered out""")
          false
        } else {
          true
        })
          //v.getBytes.length <= 990000)
      .to(SettingsFromFile.getKafkaOutputTopic)

    builder.build()
  }


  /**
   * Check if a record is from alma or CMI based on kafka message key prefix
   * @param key kafka message key
   * @return boolean
   */
  def isFromAlmaOrCMI(key: String): Boolean = {
    //exemples from kafka message keys
    //(EXLNZ-41SLSP_NETWORK)991125795889705501
    //(ZBCMI)oai:ZBcollections:dbeb348e8cb446a8a341cd49f35c03da
    key.startsWith("(EXLNZ-41SLSP_NETWORK)") || key.startsWith("(ZBCMI)")
  }
}
