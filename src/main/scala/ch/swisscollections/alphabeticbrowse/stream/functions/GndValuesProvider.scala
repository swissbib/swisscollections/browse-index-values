/*
 *
 *  * browse-index-values
 *  * Copyright (C) 2021  UB Basel
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU Affero General Public License as
 *  * published by the Free Software Foundation, either version 3 of the
 *  * License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU Affero General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU Affero General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *
 *
 */

package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.stream.functions
import ch.swisscollections.alphabeticbrowse.stream.functions.GndPatternType.{Default, GndPatternType, GndWork}
import ch.swisscollections.alphabeticbrowse.utilities.{MongoClient, MongoWrapper, XmlHelper}
import org.apache.logging.log4j.scala.Logging

import scala.util.matching.Regex




class GndValues(val gndId: String, val marcDoc: String) extends Logging {


  private val gndDefaultSplitPattern: Regex =  "<gnd>(.*?)</gnd>".r
  private val gndWorkSplitPattern: Regex =  "<gndWork>(.*?)</gndWork>".r


  def variantsValues(gndPatternType: GndPatternType = Default ): List[String] = {

    val variations =  XmlHelper.transformGndEntityVariations(marcDoc)
    /*
    the template delivers the following result (in case of available variants)
    <gnd>Hegi, F. (1774-1850)</gnd>
    <gnd>Hegj, Franz (1774-1850)</gnd>
    <gnd>Hegj (1774-1850)</gnd>
    <gnd>Hegi, François (1774-1850)</gnd>

    these variants are then provided as a List-type

     */


    gndPatternType match {
      case Default => gndDefaultSplitPattern.findAllMatchIn(variations).map(regexMatch => regexMatch.group(1)).toList
      case GndWork => gndWorkSplitPattern.findAllMatchIn(variations).map(regexMatch => regexMatch.group(1)).toList
      case _ =>
        logger.error("no valid pattern type for gndPattern Match - empty List as result")
        List[String]().empty

    }


  }

  def mainValues(gndPatternType: GndPatternType = Default): List[String] = {

    val variations = XmlHelper.transformGndEntityMain(marcDoc)

    gndPatternType match {
      case Default => gndDefaultSplitPattern.findAllMatchIn(variations).map(regexMatch => regexMatch.group(1)).toList
      case GndWork => gndWorkSplitPattern.findAllMatchIn(variations).map(regexMatch => regexMatch.group(1)).toList
      case _ =>
        logger.error("no valid pattern type for gndPattern Match - empty List as result")
        List[String]().empty

    }
  }

  def fiveHundredValues: List[String] = {
    //todo: generate the values (probably with xslt templates

    val variations: String =  XmlHelper.transformGndEntityRelations(marcDoc)

    //todo @Silvia process the relations
    List.empty[String]
  }


}


trait GndValuesProvider {
  def getGndValues(gndId: String): Option[GndValues]


}


object GndValuesProvider extends GndValuesProvider   {


  private val mongoWrapper = MongoWrapper()


  override def getGndValues(gndId: String): Option[GndValues] = {
    mongoWrapper.getGndDocument(gndId).map(doc => new GndValues(gndId, doc))
  }
}

object GndPatternType extends Enumeration {
  type GndPatternType = Value

  val Default: functions.GndPatternType.Value = Value("gnd")
  val GndWork: functions.GndPatternType.Value = Value("gndWork")

}

