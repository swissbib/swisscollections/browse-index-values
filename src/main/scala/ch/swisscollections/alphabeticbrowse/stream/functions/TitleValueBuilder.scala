/*
 *
 *  * browse-index-values
 *  * Copyright (C) 2021  UB Basel
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU Affero General Public License as
 *  * published by the Free Software Foundation, either version 3 of the
 *  * License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU Affero General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU Affero General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *
 *
 */

package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import ch.swisscollections.alphabeticbrowse.stream.functions.SubjectValueBuilder.logger
import ch.swisscollections.alphabeticbrowse.utilities.CombineWithGndIdAndVariations.cleanup
import ch.swisscollections.alphabeticbrowse.utilities.{CombineWithGndIdAndVariations, MongoWrapper, XmlHelper}
import org.apache.logging.log4j.scala.Logging

import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

object TitleValueBuilder extends Function3[String,String,GndValuesProvider,List[String]] with Logging {

  val titlePattern: Regex = "<title>(.*?)</title>".r
  val workPattern: Regex = "<work>(.*?)</work>".r

  //val test = GndValuesProvider("die ist die gnd id")

  override def apply(key: String, data: String, gndProvider: GndValuesProvider): List[String] = {
    Try[List[String]] {

      val titles: String = XmlHelper.titleValuesTermsComponent(data)

      val title: List[String] = titlePattern.findAllMatchIn(titles).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseTitleBrowse",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val work: List[String] = workPattern.findAllMatchIn(titles).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseWork",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup,
          GndPatternType.GndWork)).toList
      title ::: work
    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(exception.getMessage)
        List() //handover en empty String which is filtered out in the next operator

    }
  }
}
