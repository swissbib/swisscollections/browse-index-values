/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import ch.swisscollections.alphabeticbrowse.stream.functions.SubjectValueBuilder.logger
import ch.swisscollections.alphabeticbrowse.utilities.CombineWithGndIdAndVariations.cleanup
import ch.swisscollections.alphabeticbrowse.utilities.{CombineWithGndIdAndVariations, MongoClient, XmlHelper}
import org.apache.logging.log4j.scala.Logging

import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

object PersonValueBuilder extends Function3[String,String,GndValuesProvider,List[String]] with Logging {

  val addresseePattern: Regex = "<rcp>(.*?)</rcp>".r
  val creatorPattern: Regex = "<(aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl)>(.*?)</(aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl|org|sce)>".r
  val registrarPattern: Regex = "<(cor|col)>(.*?)</(cor|col)>".r
  val annotatorPattern: Regex = "<(ann|ins|crr|pfr)>(.*?)</(ann|ins|crr|pfr)>".r
  val arrangerPattern: Regex = "<arr>(.*?)</arr>".r
  val patronPattern: Regex = "<pat>(.*?)</pat>".r
  val autographerPattern: Regex = "<ato>(.*?)</ato>".r
  val sculptorPattern: Regex = "<scl>(.*?)</scl>".r
  val binderPattern: Regex = "<bnd>(.*?)</bnd>".r
  val performerPattern: Regex = "<(prf|cnd|itr|sng|mus)>(.*?)</(prf|cnd|itr|sng|mus)>".r
  val printerPattern: Regex = "<(pbl|prt|bsl)>(.*?)</(pbl|prt|bsl)>".r
  val platemakerPattern: Regex = "<plt>(.*?)</plt>".r
  val printmakerPattern: Regex = "<prm>(.*?)</prm>".r
  val filmmakerPattern: Regex = "<fmk>(.*?)</fmk>".r
  val photographerPattern: Regex = "<pht>(.*?)</pht>".r
  val honoreePattern: Regex = "<hnr>(.*?)</hnr>".r
  val manufacturerPattern: Regex = "<mfr>(.*?)</mfr>".r
  val illuminatorPattern: Regex = "<ilu>(.*?)</ilu>".r
  val illustratorPattern: Regex = "<ill>(.*?)</ill>".r
  val cartographerPattern: Regex = "<ctg>(.*?)</ctg>".r
  val composerPattern: Regex = "<cmp>(.*?)</cmp>".r
  val artistPattern: Regex = "<art>(.*?)</art>".r
  val lithographerPattern: Regex = "<(ltg|clt)>(.*?)</(ltg|clt)>".r
  val papermakerPattern: Regex = "<ppm>(.*?)</ppm>".r
  val praesesPattern: Regex = "<pra>(.*?)</pra>".r
  val etcherPattern: Regex = "<etr>(.*?)</etr>".r
  val respondentPattern: Regex = "<rsp>(.*?)</rsp>".r
  val facsimilistPattern: Regex = "<(fac|scr|ann|ins|crr|pfr)>(.*?)</(fac|scr|ann|ins|crr|pfr)>".r
  val narratorPattern: Regex = "<nrt>(.*?)</nrt>".r
  val engraverPattern: Regex = "<egr>(.*?)</egr>".r
  val formerOwnerPattern: Regex = "<(fmo|dnr|sll)>(.*?)</(fmo|dnr|sll)>".r
  val dedicateePattern: Regex = "<dte>(.*?)</dte>".r
  val dedicatorPattern: Regex = "<(dto|ins)>(.*?)</(dto|ins)>".r
  val compilerPattern: Regex = "<com>(.*?)</com>".r
  val ownerPattern: Regex = "<own>(.*?)</own>".r
  val depositorPattern: Regex = "<(dpt|len)>(.*?)</(dpt|len)>".r
  val itemContributorPattern: Regex = "<(ann|bnd|own|sgn|fmo|dnr|sll|dte|dto|ins|ilu|scr)>(.*?)</(ann|bnd|own|sgn|fmo|dnr|sll|dte|dto|ins|ilu|scr)>".r
  val graphicsContributorPattern: Regex = "<(egr|etr|prm|clt|ltg|plt)>(.*?)</(egr|etr|prm|clt|ltg|plt)>".r
  val agentPattern: Regex = "<agent>(.*?)</agent>".r
  val subjectAgentPattern: Regex = "<subagent>(.*?)</subagent>".r
  val nekrologPattern: Regex = "<nekrolog>(.*?)</nekrolog>".r
  val zurichPersonPattern: Regex = "<turPerson>(.*?)</turPerson>".r
  val notContributorPattern: Regex = "<(rcp|aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl|cor|col|ann|ins|crr|pfr|arr|pat|ato|scl|bnd|prf|cnd|itr|sng|mus|pbl|prt|bsl|plt|prm|fmk|pht|hnr|mfr|ilu|ill|ctg|cmp|art|ltg|clt|ppm|pra|etr|rsp|fac|scr|nrt|egr|fmo|dnr|sll|dte|dto|ins|com|own|dpt|len|agent|subagent|nekrolog|turPerson)>(.*?)</(rcp|aut|art|cmp|ctg|dub|edt|fmk|isb|lbt|lyr|pht|cre|pra|rsp|com|scl|cor|col|ann|ins|crr|pfr|arr|pat|ato|scl|bnd|prf|cnd|itr|sng|mus|pbl|prt|bsl|plt|prm|fmk|pht|hnr|mfr|ilu|ill|ctg|cmp|art|ltg|clt|ppm|pra|etr|rsp|fac|scr|nrt|egr|fmo|dnr|sll|dte|dto|ins|com|own|dpt|len|agent|subagent|nekrolog|turPerson)>".r
  val namePattern: Regex = "<.*>(.*?)</.*>".r

  override def apply(key: String, data: String, gndProvider: GndValuesProvider): List[String] = {

    import HelperFunctions._

    //import AnalyzeWrapper._

    Try[List[String]] {

      val persons: String = XmlHelper.personValuesTermsComponent(data)
      val splitPersons = persons.split("\n").toList
      val notWantedMatches = notContributorPattern.findAllMatchIn(persons).toList.map(_.toString())
      val contributorTags = splitPersons.filterNot(
        expression => notWantedMatches.contains(expression)
      ).mkString("\n")

      val contributor: List[String] = namePattern.findAllMatchIn(contributorTags).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseContributor",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val addressee: List[String] = addresseePattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseAddressee",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val creator: List[String] = creatorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseCreator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val registrar: List[String] = registrarPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseRegistrar",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val annotator: List[String] = annotatorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseAnnotator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val arranger: List[String] = arrangerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseArranger",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val patron: List[String] = patronPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePatron",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val autographer: List[String] = autographerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseAutographer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val sculptor: List[String] = sculptorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSculptor",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val binder: List[String] = binderPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBinder",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val performer: List[String] = performerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browsePerformer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val printer: List[String] = printerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browsePrinter",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val platemaker: List[String] = platemakerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePlatemaker",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val printmaker: List[String] = printmakerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePrintmaker",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val filmmaker: List[String] = filmmakerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseFilmmaker",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val photographer: List[String] = photographerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePhotographer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val honoree: List[String] = honoreePattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseHonoree",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val manufacturer: List[String] = manufacturerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseManufacturer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val illustrator: List[String] = illustratorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseIllustrator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val illuminator: List[String] = illuminatorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseIlluminator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val cartographer: List[String] = cartographerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseCartographer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val composer: List[String] = composerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseComposer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val artist: List[String] = artistPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseArtist",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val lithographer: List[String] = lithographerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseLithographer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val papermaker: List[String] = papermakerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePapermaker",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val praeses: List[String] = praesesPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePraeses",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val etcher: List[String] = etcherPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseEtcher",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val respondent: List[String] = respondentPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseRespondent",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val facsimilist: List[String] = facsimilistPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseFacsimilist",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val narrator: List[String] = narratorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseNarrator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val engraver: List[String] = engraverPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseEngraver",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val formerOwner: List[String] = formerOwnerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseFormerOwner",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val dedicatee: List[String] = dedicateePattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseDedicatee",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val dedicator: List[String] = dedicatorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseDedicator",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val compiler: List[String] = compilerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseCompiler",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val owner: List[String] = ownerPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseOwner",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val depositor: List[String] = depositorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseDepositor",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val itemContributor: List[String] = itemContributorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseItemContributor",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val graphicsContributor: List[String] = graphicsContributorPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseGraphicsContributor",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val agent: List[String] = agentPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseAgent",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val subjectAgent: List[String] = subjectAgentPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSubjectAgent",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val nekrolog: List[String] = nekrologPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseNekrolog",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val zurichPerson: List[String] = zurichPersonPattern.findAllMatchIn(persons).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseZurichPerson",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.removeAngleBrackets(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      addressee ::: creator ::: registrar ::: annotator ::: arranger ::: patron ::: autographer ::: sculptor :::
        binder ::: performer ::: printer ::: platemaker  ::: printmaker ::: filmmaker ::: photographer :::
        honoree ::: manufacturer ::: illustrator ::: illuminator ::: cartographer ::: composer ::: artist  :::
        lithographer ::: papermaker ::: praeses ::: etcher ::: respondent ::: facsimilist ::: narrator :::
        engraver ::: formerOwner ::: dedicatee ::: dedicator ::: compiler ::: owner ::: depositor :::
        agent ::: subjectAgent ::: nekrolog ::: zurichPerson ::: contributor ::: itemContributor ::: graphicsContributor

    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(s"""exception in PersonValueBuilder: ${exception.getMessage}""")
        List() //handover en empty String which is filtered out in the next operator

    }
  }


}
