/*
 * browse-index-values
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.stream.functions


import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper.{Replacement, padding, regexList}




object HelperFunctions {

  private def regexList = List (
    Replacement("""&lt;&lt;(.*)&gt;&gt;""".r,"$1"),
  )

  /*
  val regex2stringGroup1: (Regex,String) => Option[String] = (regex, value) => {
    regex.findFirstIn(value).map(_)

  }

   */

  val formatRawData: String => String = raw =>
  //todo: better string formatting....
    if (raw.length > 50) {
      raw.substring(0,50)
    } else {
      padding.format(raw)
    }


  val cleanValue: String => String = {
    rawData =>
      regexList.foldLeft(rawData)((str, step) => {
        step.reg.replaceAllIn(str, step.replacement)
      })
  }

  val formatTwoValue: ((String,String)  => String) = (firstValue,secondValue) =>

    s"""${firstValue}${"x"*10}${secondValue}"""


}
