/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.stream.functions

import java.nio.charset.Charset

import org.apache.commons.codec.binary.Base64
import ch.swisscollections.alphabeticbrowse.normalizer.ICUCollatorNormalizer
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import scala.xml.XML


object TitleKeyValueBuilder extends Function2[String,String,String] with Logging {

  private lazy val normalizer = new ICUCollatorNormalizer
  //private val KEY_SEPARATOR = "\1"
  private val KEY_SEPARATOR = "\u0001"
  private val RECORD_SEPARATOR = "\r\n"


  override def apply(key: String, data: String): String = {

    Try[String] {
      val xml = XML.loadString(data)

      //val title = (xml \ "browsevalues" \ "title").text

      //val ff = xml \\ "title"

      val title = (xml \ "title").text
      val normalizedSortKey = normalizer.normalize(title)
      //val normalizedTitle = normalizer.normalize(title)

      //todo: welche Faelle gibt es, in denen key und heading (also der Anzeigetext)
      // unterschiedlich sind
      new String(Base64.encodeBase64(normalizedSortKey)) +
        KEY_SEPARATOR +
        new String(Base64.encodeBase64(title.getBytes(Charset.forName("UTF-8")))) +
        KEY_SEPARATOR +
        new String(Base64.encodeBase64(title.getBytes(Charset.forName("UTF-8"))))

      //val backToString = new String(normalizedTitle, StandardCharsets.UTF_8)



    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(s"""exception in TitleKeyValueBuilder: ${exception.getMessage}""")
        "" //handover en empty String which is filtered out in the next operator

    }



  }

}
