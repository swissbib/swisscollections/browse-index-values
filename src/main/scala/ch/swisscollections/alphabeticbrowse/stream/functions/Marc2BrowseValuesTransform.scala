/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.utilities.XmlHelper
import scala.util.{Failure, Success, Try}
import scala.xml.{Node, XML}
import org.apache.logging.log4j.scala.Logging


object Marc2BrowseValuesTransform extends Function2[String,String,String] with Logging {


  override def apply(v1: String, v2: String): String = {

    //logger.info(s"transformation of $v2")

    Try[String] {

      val browseValues = XmlHelper.transformIntoBrowseValues(v2)
      XmlHelper.makeXmlOneLiner(
        XML.loadString(browseValues)
      )

    } match {
      case Success(value) => value
      case Failure(exception) =>
        println(s"""in println (Marc2BrowseValuesTransform): ${exception.getMessage}""")
        logger.error(exception.getMessage)
        "" //handover en empty String which is filtered out in the next operator

    }
  }
}
