/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import ch.swisscollections.alphabeticbrowse.utilities.CombineWithGndIdAndVariations.cleanup
import ch.swisscollections.alphabeticbrowse.utilities.{CombineWithGndIdAndVariations, MongoClient, XmlHelper}
import org.apache.logging.log4j.scala.Logging

import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

object SubjectValueBuilder extends Function3[String,String,GndValuesProvider,List[String]] with Logging {

  val initiumPattern: Regex = "<han-A1>(.*?)</han-A1>".r
  val prayerPattern: Regex = "<han-A2>(.*?)</han-A2>".r
  val iconographyPattern: Regex = "<han-A3>(.*?)</han-A3>".r
  val repertoriumPattern: Regex = "<han-A4>(.*?)</han-A4>".r
  val bernClassificationPattern: Regex = "<ube-GA>(.*?)</ube-GA>".r
  val bernPlacePattern: Regex = "<ube-BC>(.*?)</ube-BC>".r
  val bernPersonPattern: Regex = "<ube-BD>(.*?)</ube-BD>".r
  val bernTopicPattern: Regex = "<ube-BE>(.*?)</ube-BE>".r
  val baselClassificationPattern: Regex = "<ubs-FA>(.*?)</ubs-FA>".r
  val baselPersonPlacePattern: Regex = "<ubs-AC>(.*?)</ubs-AC>".r
  val formImagePattern: Regex = "<(uzb-ZG|imageForm)>(.*?)</(uzb-ZG|imageForm)>".r
  val timePattern: Regex = "<(gndTime|stwTime)>(.*?)</(gndTime|stwTime)>".r
  val mediumPattern: Regex = "<(idsmusi)>(.*?)</(idsmusi)>".r
  val topicPattern: Regex = "<(gndTopic|stwTopic|gnd-contentForm)>(.*?)</(gndTopic|stwTopic|gnd-contentForm)>".r
  val geoPattern: Regex = "<(gndGeo|stwGeo|ube-BC|geoAC)>(.*?)</(gndGeo|stwGeo|ube-BC|geoAC)>".r
  val zurichClassificationPatten: Regex = "<tur>(.*?)</tur>".r
  val zurichGeoPatten: Regex = "<turGeo>(.*?)</turGeo>".r
  val placePattern: Regex = "<(production|publication|geoPublication)>(.*?)</(production|publication|geoPublication)>".r
  val datePattern: Regex = "<date>(.*?)</date>".r
  val musicGenrePattern: Regex = "<(idsmusg|idsadaption)>(.*?)</(idsmusg|idsadaption)>".r
  val solothurnGeoPattern: Regex = "<geoSobib>(.*?)</geoSobib>".r
  val solothurnAgentPattern: Regex = "<agentSobib>(.*?)</agentSobib>".r

  override def apply(key: String, data: String, gndProvider: GndValuesProvider): List[String] = {
    Try[List[String]] {

      val subjects: String = XmlHelper.subjectValuesTermsComponent(data)
      val initium: List[String] = initiumPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseInitium",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplaceStar"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val prayer: List[String] = prayerPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browsePrayer",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val iconography: List[String] = iconographyPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseIconography",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val repertorium: List[String] = repertoriumPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseRepertorium",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val bernClassification: List[String] = bernClassificationPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBernClassification",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = ""),
          cleanUpPresentationValueFunction = cleanup)).toList

      //val bernClassification = bernClassificationPattern.findAllMatchIn(subjects).map(regexMatch =>
      //  s"""<field name="browseBernClassification">${AnalyzeWrapper.generalAnalyzer(regexMatch.group(1))}</field>""").toList

      val bernPlace: List[String] = bernPlacePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBernPlace",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val bernPerson: List[String] = bernPersonPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBernPerson",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val bernTopic: List[String] = bernTopicPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBernTopic",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val baselClassification: List[String] = baselClassificationPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBaselClassification",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = ""),
          cleanUpPresentationValueFunction = cleanup)).toList

      val baselPersonPlace: List[String] = baselPersonPlacePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseBaselPersonPlace",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val formImage: List[String] = formImagePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseFormImage",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val time: List[String] = timePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseTime",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val medium: List[String] = mediumPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseMedium",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val topic: List[String] = topicPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseTopicBrowse",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val geo: List[String] = geoPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseGeo",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val zurichClassification: List[String] = zurichClassificationPatten.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseZurichClassification",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = ""),
          cleanUpPresentationValueFunction = cleanup)).toList

      val zurichGeo: List[String] = zurichGeoPatten.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseZurichGeo",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val place: List[String] = placePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browsePlace",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val date: List[String] = datePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseDate",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = ""),
          cleanUpPresentationValueFunction = cleanup)).toList

      val musicGenre: List[String] = musicGenrePattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseMusicGenre",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val solothurnGeo: List[String] = solothurnGeoPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSolothurnGeo",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val solothurnAgent: List[String] = solothurnAgentPattern.findAllMatchIn(subjects).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSolothurnAgent",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "patternReplace"),
          cleanUpPresentationValueFunction = cleanup)).toList

      initium ::: prayer ::: iconography ::: repertorium ::: bernClassification ::: bernPlace ::: bernPerson :::
        bernTopic ::: baselClassification ::: baselPersonPlace ::: formImage ::: time ::: medium ::: topic :::
        geo ::: zurichClassification ::: zurichGeo ::: place ::: date ::: musicGenre ::: solothurnGeo ::: solothurnAgent
    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(s"""exception in SubjectValueBuilder: ${exception.getMessage}""")
        List() //handover en empty String which is filtered out in the next operator

    }
  }
}
