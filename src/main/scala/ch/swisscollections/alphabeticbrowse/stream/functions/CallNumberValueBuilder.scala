/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.alphabeticbrowse.stream.functions

import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper
import ch.swisscollections.alphabeticbrowse.utilities.CombineWithGndIdAndVariations.cleanup
import ch.swisscollections.alphabeticbrowse.utilities.{CombineWithGndIdAndVariations, XmlHelper}
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import scala.util.matching.Regex

object CallNumberValueBuilder extends Function3[String,String,GndValuesProvider,List[String]] with Logging {

  val formerCallNumberPattern: Regex = "<han-A5>(.*?)</han-A5>".r
  val AKBCallNumberPattern: Regex = "<AKB>(.*?)</AKB>".r
  val A100CallNumberPattern: Regex = "<A100>(.*?)</A100>".r
  val A125CallNumberPattern: Regex = "<A125>(.*?)</A125>".r
  val B583CallNumberPattern: Regex = "<B583>(.*?)</B583>".r
  val A380CallNumberPattern: Regex = "<A380>(.*?)</A380>".r
  val A381CallNumberPattern: Regex = "<A381>(.*?)</A381>".r
  val LUZHBCallNumberPattern: Regex = "<(LUZHB|LUSBI)>(.*?)</(LUZHB|LUSBI)>".r
  val A150CallNumberPattern: Regex = "<A150>(.*?)</A150>".r
  val SGKBVCallNumberPattern: Regex = "<SGKBV>(.*?)</SGKBV>".r
  val SGSTICallNumberPattern: Regex = "<SGSTI>(.*?)</SGSTI>".r
  val SGARKCallNumberPattern: Regex = "<SGARK>(.*?)</SGARK>".r
  val ZBZCallNumberPattern: Regex = "<(Z01|Z02|Z03|Z04|Z05|Z06|Z07|Z08)>(.*?)</(Z01|Z02|Z03|Z04|Z05|Z06|Z07|Z08)>".r
  val A382CallNumberPattern: Regex = "<A382>(.*?)</A382>".r
  val UBECallNumberPattern: Regex = "<(B400|B404|B410|B412|B415|B452|B464|B465|B466|B467|B500|B521|B542|B552|B554|B555|B589|E30)>(.*?)</(B400|B404|B410|B412|B415|B452|B464|B465|B466|B467|B500|B521|B542|B552|B554|B555|B589|E30)>".r
  val allCallNumberPattern: Regex = "<(han-A5|AKB|A100|A125|B583|A380|A381|LUZHB|LUSBI|A150|SGKBV|SGSTI|SGARK|Z01|Z02|Z03|Z04|Z05|Z06|Z07|Z08|A382|B400|B404|B410|B412|B415|B452|B464|B465|B466|B467|B500|B521|B542|B552|B554|B555|B589|E30)>(.*?)</(han-A5|AKB|A100|A125|B583|A380|A381|LUZHB|LUSBI|A150|SGKBV|SGSTI|SGARK|Z01|Z02|Z03|Z04|Z05|Z06|Z07|Z08|A382|B400|B404|B410|B412|B415|B452|B464|B465|B466|B467|B500|B521|B542|B552|B554|B555|B589|E30)>".r

  override def apply(key: String, data: String, gndProvider: GndValuesProvider): List[String] = {

    Try[List[String]] {

      val callNumbers: String = XmlHelper.callNumberValuesTermsComponent(data)

      val formerCallNumber: List[String] = formerCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseFormerCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val AKBCallNumber: List[String] = AKBCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseAKBCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A100CallNumber: List[String] = A100CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA100CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A125CallNumber: List[String] = A125CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA125CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val B583CallNumber: List[String] = B583CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseB583CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A380CallNumber: List[String] = A380CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA380CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A381CallNumber: List[String] = A381CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA381CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val LUZHBCallNumber: List[String] = LUZHBCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseLUZHBCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A150CallNumber: List[String] = A150CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA150CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val SGKBVCallNumber: List[String] = SGKBVCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSGKBVCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val SGSTICallNumber: List[String] = SGSTICallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSGSTICallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val SGARKCallNumber: List[String] = SGARKCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseSGARKCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val ZBZCallNumber: List[String] = ZBZCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseZBZCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val A382CallNumber: List[String] = A382CallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(1),
          endPoint = "browseA382CallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val UBECallNumber: List[String] = UBECallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseUBECallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      val allCallNumber: List[String] = allCallNumberPattern.findAllMatchIn(callNumbers).flatMap(regexMatch =>
        CombineWithGndIdAndVariations(
          bibValue =  regexMatch.group(2),
          endPoint = "browseAllCallNumber",
          gndValuesProvider=  gndProvider,
          rawTermAnalyzerFunction = AnalyzeWrapper.generalAnalyzer(
            _,
            analyzerName = "callNumberAnalyzer"),
          cleanUpPresentationValueFunction = cleanup)).toList

      formerCallNumber ::: AKBCallNumber ::: A100CallNumber ::: A125CallNumber ::: B583CallNumber :::
        A380CallNumber ::: A381CallNumber ::: LUZHBCallNumber ::: A150CallNumber ::: SGKBVCallNumber :::
        SGSTICallNumber ::: SGARKCallNumber ::: ZBZCallNumber ::: A382CallNumber ::: UBECallNumber ::: allCallNumber

    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.error(s"""exception in CallNumberValueBuilder: ${exception.getMessage}""")
        List() //handover en empty String which is filtered out in the next operator

    }

  }


}
