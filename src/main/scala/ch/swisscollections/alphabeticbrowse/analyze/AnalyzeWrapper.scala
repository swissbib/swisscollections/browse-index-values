/*
 *
 *  * browse-index-values
 *  * Copyright (C) 2021  UB Basel
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU Affero General Public License as
 *  * published by the Free Software Foundation, either version 3 of the
 *  * License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU Affero General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU Affero General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *
 *
 */

package ch.swisscollections.alphabeticbrowse.analyze

import ch.swisscollections.alphabeticbrowse.analyzers.{BasicAnalyzer, CallNumberAnalyzer, PatternReplaceAnalyzer, PatternReplaceStarAnalyzer}
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute

import scala.collection.immutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.util.matching.Regex

object AnalyzeWrapper {

  val padding = "%1$-50s"
  private lazy val analyzerMap = HashMap (
    "basicAnalyzer" -> new BasicAnalyzer,
    "patternReplace" -> new PatternReplaceAnalyzer,
    "patternReplaceStar" -> new PatternReplaceStarAnalyzer,
    "callNumberAnalyzer" -> new CallNumberAnalyzer
  )

  case class Replacement(reg: Regex, replacement: String)


  //habe zuerst eine Map verwendet, hier kässt sich aber die Fold Operation (s.u.)
  //nicht so einsetzen. Deswegen unten der List Typ
  //ich denke dass in List die Sortierung der Reihenfolge des Ertsllens entspricht
  //einfach ausprobieren oder testen
  /*
  private lazy val regexMap = HashMap (
    "removeAngleBrackets" -> Replacement("&lt;&lt;(.*)&gt;&gt;".r,"$1"),
  )
*/

  //ich denke man muss das in """ """ setzen
  //ansonsten bekommt man ein Problem mit escapes, bitte ausprobieren
  private def regexList = List (
    Replacement("""&lt;&lt;(.*)&gt;&gt;""".r,"$1"),
  )


  def formatRawData(raw: String): String =
  //todo: better string formatting....
    if (raw.length > 50) {
      raw.substring(0,50)
    } else {
      padding.format(raw)
    }

  def generalAnalyzer(rawData: String, analyzerName: String = "basicAnalyzer" ): String = {

    val analyzer = analyzerMap.getOrElse(analyzerName, analyzerMap("basicAnalyzer"))
    val ts = analyzer.tokenStream("fieldNotNeeded",rawData)

    val chars = ArrayBuffer[String]()

    ts.reset()
    while (ts.incrementToken()) {
      chars.append(ts.getAttribute(classOf[CharTermAttribute]).toString)

    }
    ts.close()

    //val temp = s"""${formatRawData(chars.mkString)}${"x"*10}${rawData}"""
    //temp
    chars.mkString
  }

  def removeAngleBrackets(rawData: String, analyzerName: String = "basicAnalyzer", regexName: String = "regexList"): String = {

    val analyzer = analyzerMap.getOrElse(analyzerName, analyzerMap("basicAnalyzer"))
    val ts = analyzer.tokenStream("fieldNotNeeded",rawData)

    val chars = ArrayBuffer[String]()

    ts.reset()
    while (ts.incrementToken()) {
      chars.append(ts.getAttribute(classOf[CharTermAttribute]).toString)

    }
    ts.close()


    //val clean = regexList.foldLeft(rawData)((str, step) => {
    //  step.reg.replaceAllIn(str, step.replacement)
    //})




    //val temp = s"""${formatRawData(chars.mkString)}${"x"*10}${clean}"""
    //temp


    chars.mkString
  }


}