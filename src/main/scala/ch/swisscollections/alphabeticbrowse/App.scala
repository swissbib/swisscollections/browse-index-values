/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse

import java.time.Duration
import java.util.UUID

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}

object App extends App with Logging {

  val topology = new KafkaTopology


  //val fullConf = AppProperties.plugins.fulltextConf
  //val gndConf = AppProperties.plugins.gndConf

  val propsKafkaSettings = SettingsFromFile.getKafkaStreamsSettings


  //read always from beginning
  //props.put("application.id",UUID.randomUUID().toString)
  //println(props)
  val streams = new KafkaStreams(topology.build(), propsKafkaSettings)
  //val streams = new KafkaStreams(topology.build(), SettingsFromFile.getKafkaStreamsSettings)

  val shutdownGracePeriodMs = 10000

  logger.trace("Starting stream processing")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Workflow successful. Finishing...")
    case Failure(f) =>
      logger.error(s"Aborting due to errors: ${f.getMessage}")
      sys.exit(1)
  }

  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }


}
