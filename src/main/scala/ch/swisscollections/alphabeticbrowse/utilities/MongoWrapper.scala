/*
 * browse-index-values (swisscollections)
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package ch.swisscollections.alphabeticbrowse.utilities


import ch.swisscollections.alphabeticbrowse.SettingsFromFile
import ch.swisscollections.alphabeticbrowse.utilities.MongoWrapper.{deserialize, gndIdRegex}
import com.mongodb.BasicDBObject
import com.mongodb.client.{MongoClients, MongoDatabase}
import com.mongodb.client.MongoCollection
import org.bson.Document

import java.io.ByteArrayOutputStream
import java.util.zip.Inflater
import scala.util.matching.Regex


trait MongoClient {
  def getGndDocument(gndId: String) :Option[String]
}


class MongoWrapper(private val collection: MongoCollection[Document]) extends MongoClient {


  def getGndDocument(gndId: String) :Option[String] = {
    /*
    bib record delivers gnd numbers in the form (DE-588)118702742
    the _id of the gnd documents in our local is are in the format:
    (DNBGND)oai:dnb.de/authorities/1101472278
    so dynamic is the last part
     */

    //val id = s"""(DNBGND)oai:dnb.de/authorities/${gndNr}"""
    val bdbo = new BasicDBObject()
    bdbo.put("gndid",  gndId)

    Option(collection.find(bdbo).first()) match {
      case Some(doc) =>
        Option(deserialize(doc.get("record").asInstanceOf[org.bson.types.Binary].getData))
      case None => Option.empty[String]
    }

    /*
    gndIdRegex.findFirstMatchIn(gndId).map(_.group(1)) match {
      case Some(gndNr) =>
        val id = s"""(DNBGND)oai:dnb.de/authorities/${gndNr}"""
        val bdbo = new BasicDBObject()
        bdbo.put("_id",  id)

        Option(collection.find(bdbo).first()) match {
          case Some(doc) =>
            Option(deserialize(doc.get("record").asInstanceOf[org.bson.types.Binary].getData))
          case None => Option.empty[String]
        }

      case None => Option.empty[String]
    }

     */
  }
}

object MongoWrapper {

  private val byteArrayLength = 8192

  private var mongoWrapper: Option[MongoWrapper] = Option.empty

  val gndIdRegex: Regex = """\(DE-588\)(.*$)""".r

  def apply (): MongoWrapper = {


    mongoWrapper.getOrElse {
      val mongoJavaClient = MongoClients.create(SettingsFromFile.getMongoConnection);

      val hanDB: MongoDatabase = mongoJavaClient.getDatabase(SettingsFromFile.getMongoDB)


      val collection: MongoCollection[Document] = hanDB.getCollection(SettingsFromFile.getMongoCollection)

      //todo find a possibility how to check if we have a valid connection???
      mongoWrapper = Option(new MongoWrapper(collection))

      mongoWrapper.get

    }
  }

  def deserialize(data: Array[Byte]): String = {

    val decompressor = new Inflater()
    decompressor.setInput(data)
    val bos = new ByteArrayOutputStream(data.length)
    val buffer = new Array[Byte](byteArrayLength)
    while (!decompressor.finished) {

      val size = decompressor.inflate(buffer)
      bos.write(buffer, 0, size)
    }
    bos.toString


  }

}
