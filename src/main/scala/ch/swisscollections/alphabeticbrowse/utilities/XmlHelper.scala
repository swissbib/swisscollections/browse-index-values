/*
 * browse-index-values
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.utilities

import ch.swisscollections.alphabeticbrowse.SettingsFromFile
import ch.swisscollections.alphabeticbrowse.helper.{TemplateCreator, TemplateTransformer}
import javax.xml.transform.Transformer

import scala.collection.mutable
import scala.xml.Node

object XmlHelper {

  lazy val leechTransformer: Transformer = {
    new TemplateCreator("templates/leech.xsl")
      .createTransformerFromPath()
  }

  lazy val personTransformerTermsComponent: Transformer = {
    new TemplateCreator("templates/personTermsComponent.xslt")
      .createTransformerFromPath()
  }

  lazy val gndRelatedEntities: Transformer = {
    new TemplateCreator("templates/gnd500.xsl")
      .createTransformerFromPath()
  }

  lazy val gndEntityVariations: Transformer = {
    new TemplateCreator( "templates/gndVariants.xslt")
      .createTransformerFromPath()
  }

  lazy val gndEntityMain: Transformer = {
    new TemplateCreator("templates/gndMain.xslt")
      .createTransformerFromPath()
  }

  lazy val titleTransformerTermsComponent: Transformer = {
    new TemplateCreator("templates/titleTermsComponent.xslt")
      .createTransformerFromPath()
  }

  lazy val subjectTransformerTermsComponent: Transformer = {
    new TemplateCreator("templates/subjectTermsComponent.xslt")
      .createTransformerFromPath()
  }

  lazy val callNumberTransformerTermsComponent: Transformer = {
    new TemplateCreator("templates/callNumberTermsComponent.xslt")
      .createTransformerFromPath()
  }

  lazy val removeAllNSTransformer: Transformer = {
    new TemplateCreator("templates/removeallns.xsl")
      .createTransformerFromPath()
  }


  def transformIntoBrowseValues(xml: String): String =
    new TemplateTransformer(xml).transform(leechTransformer)

  def personValuesTermsComponent(xml: String): String =
    new TemplateTransformer(xml).transform(personTransformerTermsComponent)

  def titleValuesTermsComponent(xml: String): String =
    new TemplateTransformer(xml).transform(titleTransformerTermsComponent)

  def subjectValuesTermsComponent(xml: String): String =
    new TemplateTransformer(xml).transform(subjectTransformerTermsComponent)

  def callNumberValuesTermsComponent(xml: String): String =
    new TemplateTransformer(xml).transform(callNumberTransformerTermsComponent)

  def transformNoNS(xml: String): String =
    new TemplateTransformer(xml).transform(removeAllNSTransformer)

  def transformGndEntityRelations(xml: String): String =
    new TemplateTransformer(xml).transform(gndRelatedEntities)

  def transformGndEntityVariations(xml: String): String =
    new TemplateTransformer(xml).transform(gndEntityVariations)

  def transformGndEntityMain(xml: String): String =
    new TemplateTransformer(xml).transform(gndEntityMain)


  def makeXmlOneLiner(xml: Node): String = {
    val p = new scala.xml.PrettyPrinter(0, 0, true)
    val sB = new mutable.StringBuilder
    p.format(xml, sB)
    sB.toString()
  }



}
