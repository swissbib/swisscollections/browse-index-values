/*
 * browse-index-values
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.alphabeticbrowse.utilities


import ch.swisscollections.alphabeticbrowse.stream.functions.GndPatternType.{Default, GndPatternType}
import ch.swisscollections.alphabeticbrowse.stream.functions.{GndValues, GndValuesProvider}

import scala.util.matching.Regex

object CombineWithGndIdAndVariations extends ((String, String, GndValuesProvider, String => String, String => String, GndPatternType) => List[String]) {

  case class Replacement(reg: Regex, replacement: String)

  private def regexList = List (
    Replacement("""&lt;&lt;(.*)&gt;&gt;""".r,"$1"),
    Replacement("""&#x98;(.*)&#x9c;""".r,"$1"),
  )

  val padding = "%1$-50s"
  def formatRawData(raw: String): String =
  //todo: better string formatting....
    if (raw.length > 50) {
      raw.substring(0,50)
    } else {
      padding.format(raw)
    }

  val paddingNoFilling = "%1"
  def formatRawDataNoFilling(raw: String): String = raw


  val patternConcatenationNoFilling: (String, Int, String) => String =
    (part1,length,part2) => {
      s"""${formatRawDataNoFilling(part1)}${"#" * length}${part2}"""
    }


  val patternConcatenation: (String, Int, String) => String =
    (part1,length,part2) => {
      s"""${formatRawData(part1)}${"#" * length}${part2}"""
  }

  private val patternDefaultLength10Concatenation = patternConcatenation(_,10,_)
  private val patternDefaultLength10ConcatenationNoFilling = patternConcatenationNoFilling(_,10,_)

  val cleanup: String => String = (rawData:String) =>  regexList.foldLeft(rawData)((str, step) => {
    step.reg.replaceAllIn(str, step.replacement)
  })




  val gndMatchPattern = "##xx##"

  override def apply(bibValue: String,
                     endPoint: String,
                     gndValuesProvider: GndValuesProvider,
                     rawTermAnalyzerFunction: String => String,
                     cleanUpPresentationValueFunction: String => String,
                     gndPatternType: GndPatternType = Default
                    ): List[String] = {
    //import ch.swisscollections.alphabeticbrowse.analyze.AnalyzeWrapper.formatRawData

    val splittedValue = bibValue.split(gndMatchPattern)
    splittedValue.length match {
      case 1 => //no GndId available
        val rawValue = splittedValue(0)
        List(s"""<field name="${endPoint}">${patternDefaultLength10Concatenation(
          rawTermAnalyzerFunction(splittedValue(0)),cleanUpPresentationValueFunction(rawValue))}</field>""")
        //List(s"""<field name="${endPoint}">${formatRawData()rawTermAnalyzerFunction(splittedValue(0))}</field>""")
      case 2 =>
        //example complete main Entry
        // "mozart anna maria 1720 1778                       xxxxxxxxxxMozart, Anna Maria (1720-1778)       xxxxxxxxxx(DE-588)119285428",
        val rawValue = splittedValue(0)
        val gnd = splittedValue(1)
        val mainEntryWithGndNotAnalyzed = patternDefaultLength10ConcatenationNoFilling(cleanUpPresentationValueFunction(rawValue),gnd.trim)
        val completeMainEntry = List(s"""<field name="${endPoint}">${patternDefaultLength10Concatenation(
          rawTermAnalyzerFunction(splittedValue(0)),mainEntryWithGndNotAnalyzed)}</field>""")


        val rawSideVariants = (for {
          gndValues: GndValues <- gndValuesProvider.getGndValues(gnd.trim)
          variants =  gndValues.variantsValues(gndPatternType).map {identity}
        } yield variants).getOrElse(List.empty)

        //example with side variants
        //"pertl maria anna 1720 1778                        xxxxxxxxxxPertl, Maria Anna (1720-1778)        xxxxxxxxxx(DE-588)119285428        xxxxxxxxxxMozart, Anna Maria (1720-1778)",
        val analyzedVariantValues = rawSideVariants.map {
          variantValue =>
            val partGndWithMainEntry = patternDefaultLength10ConcatenationNoFilling(gnd,cleanUpPresentationValueFunction(rawValue))
            val cleanUpSideVariantWithMainEntry = patternDefaultLength10ConcatenationNoFilling(
              cleanUpPresentationValueFunction(variantValue),
              partGndWithMainEntry)
            val completeValue = patternDefaultLength10Concatenation(
              rawTermAnalyzerFunction(variantValue),
              cleanUpSideVariantWithMainEntry)
            s"""<field name="${endPoint}">${completeValue}</field>"""
        }
        completeMainEntry ::: analyzedVariantValues

      case 3 =>
        val gnd = splittedValue(1)

        val mainEntryFromGnd = (for {
          gndValues: GndValues <- gndValuesProvider.getGndValues(gnd.trim)
          main = gndValues.mainValues(gndPatternType).map {
            identity
          }
        } yield main).getOrElse(List.empty)

        val mainEntryWithGndNotAnalyzed = patternDefaultLength10ConcatenationNoFilling(cleanUpPresentationValueFunction(mainEntryFromGnd.head), gnd.trim)
        val completeMainEntry = List(
          s"""<field name="${endPoint}">${
            patternDefaultLength10Concatenation(
              rawTermAnalyzerFunction(mainEntryFromGnd.head), mainEntryWithGndNotAnalyzed)
          }</field>""")

        val rawSideVariants = (for {
          gndValues: GndValues <- gndValuesProvider.getGndValues(gnd.trim)
          variants = gndValues.variantsValues(gndPatternType).map {
            identity
          }
        } yield variants).getOrElse(List.empty)

        val analyzedVariantValues = rawSideVariants.map {
          variantValue =>
            val partGndWithMainEntry = patternDefaultLength10ConcatenationNoFilling(gnd, cleanUpPresentationValueFunction(mainEntryFromGnd.head))
            val cleanUpSideVariantWithMainEntry = patternDefaultLength10ConcatenationNoFilling(
              cleanUpPresentationValueFunction(variantValue),
              partGndWithMainEntry)
            val completeValue = patternDefaultLength10Concatenation(
              rawTermAnalyzerFunction(variantValue),
              cleanUpSideVariantWithMainEntry)
            s"""<field name="${endPoint}">${completeValue}</field>"""
        }
        completeMainEntry ::: analyzedVariantValues

      case _ =>
        //should not be possible
        ???


    }

  }
}
