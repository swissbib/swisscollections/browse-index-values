import Dependencies._

ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "UB Basel"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}


lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "browse-index-values",
    //scalacOptions += "-Ypartial-unification",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.swisscollections.alphabeticbrowse.App"),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      kafkaStreams,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils excludeAll (ExclusionRule(organization =
        "org.slf4j"
      )),
      scalatic,
      //uPickle,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      scala_xml,
      saxon,
      lucenecore,
      luceneAnalyzersCommon,
      luceneICU,
      luceneMisc,
      commonsCodec,
      mongoJavaClientCurrent


  )
  )
