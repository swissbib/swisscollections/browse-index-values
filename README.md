# Browse Index Values

This code creates the solr records for the swisscollections "Index Search"

## Test locally with kafka cluster

make up (will start a kafka cluster via docker-compose) and a mongo connection to an empty mongo

Create the topics and the test data
```
./scripts/create-test-data.sh
```

Set the following environment variables

```
APPLICATION_ID=browse-values
KAFKA_BOOTSTRAP_SERVERS=localhost:29092
TOPIC_IN=swisscollections-slsp-deduplicated
TOPIC_OUT=swisscollections-browse-values
MONGO_COLLECTION=sourceDNBGND1
MONGO_DB=nativeSources
MONGO_URI=mongodb://admin:XXXXXXXXXX@localhost:27013
```


Run the program (click the green arrow in App.scala in IntelliJ) or
```
sbt run
```

Check what is written in swisscollections-browse-values (use the kafka plugin in intellij to read from topics). Should be something like this (with an empty mongo)

```
<doc>
	<field name="id">991094525059705501</field>
	<field name="browseCreator">la roche andreas 1757 1817                        ##########La Roche, Andreas (1757-1817)##########(DE-588)106464399X</field>
	<field name="browsePrinter">haas wilhelm 1766 1838                            ##########Haas, Wilhelm (1766-1838)##########(DE-588)11955593X</field>
	<field name="browseHonoree">raillard margaretha 1750 1791                     ##########Raillard, Margaretha (1750-1791)##########(DE-588)1295776367</field>
	<field name="browseAgent">la roche andreas 1757 1817                        ##########La Roche, Andreas (1757-1817)##########(DE-588)106464399X</field>
	<field name="browseAgent">raillard margaretha 1750 1791                     ##########Raillard, Margaretha (1750-1791)##########(DE-588)1295776367</field>
	<field name="browseAgent">haas wilhelm 1766 1838                            ##########Haas, Wilhelm (1766-1838)##########(DE-588)11955593X</field>
	<field name="browseNekrolog">raillard margaretha 1750 1791                     ##########Raillard, Margaretha (1750-1791)##########(DE-588)1295776367</field>
	<field name="browseTitleBrowse">leichenrede uber psalm cxxvi 5 6 bey der beerdigun##########Leichenrede über Psalm CXXVI. 5. 6. bey der Beerdigung der ... Margaretha Raillard : Freytags den 14. Jenner 1791 im Münster gehalten</field>
	<field name="browseWork">la roche andreas 1757 1817 leichenrede uber psalm ##########La Roche, Andreas (1757-1817) / Leichenrede über Psalm CXXVI. 5. 6. bey der Beerdigung der ... Margaretha Raillard : Freytags den 14. Jenner 1791 im Münster gehalten</field>
	<field name="browseTopicBrowse">nachruf                                           ##########Nachruf</field>
	<field name="browsePlace">basel                                             ##########Basel</field>
	<field name="browsePlace">basel                                             ##########Basel</field>
	<field name="browseA100CallNumber">ubh                                               ##########UBH</field>
	<field name="browseA100CallNumber">ubh kiar g x 0000084  0000002  0000007            ##########UBH KiAr G X 84:2:7</field>
	<field name="browseA100CallNumber">ubh                                               ##########UBH</field>
	<field name="browseA100CallNumber">ubh kiar g x 0000071  0000012                     ##########UBH KiAr G X 71:12</field>
	<field name="browseA100CallNumber">ubh                                               ##########UBH</field>
	<field name="browseAllCallNumber">ubh                                               ##########UBH</field>
	<field name="browseAllCallNumber">ubh kiar g x 0000084  0000002  0000007            ##########UBH KiAr G X 84:2:7</field>
	<field name="browseAllCallNumber">ubh                                               ##########UBH</field>
	<field name="browseAllCallNumber">ubh kiar g x 0000071  0000012                     ##########UBH KiAr G X 71:12</field>
	<field name="browseAllCallNumber">ubh                                               ##########UBH</field>
</doc>
```



See readme in slsp-deduplication for more background

