<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="fn">
    <xsl:import href="sharedTemplates.xslt"/>
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            todo: write something useful
        </desc>
    </doc>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>


    <xsl:template match="/">

        <xsl:apply-templates/>


    </xsl:template>

    <xsl:template match="datafield[@tag='100'] |
                         datafield[@tag='600' and matches(subfield[@code='2'], '^gnd$|^stw$')] |
                         datafield[@tag='700']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:call-template name="browse_person"/>
                <xsl:element name="agent">
                    <xsl:call-template name="person_name"/>
                    <xsl:call-template name="append_gndid"/>
                    <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                        <xsl:text>##xx##ZBC</xsl:text>
                    </xsl:if>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='110'] |
                         datafield[@tag='610' and matches(subfield[@code='2'], '^gnd$|^stw$')] |
                         datafield[@tag='710']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:call-template name="browse_corporate"/>
                <xsl:element name="agent">
                    <xsl:call-template name="corporate_name"/>
                    <xsl:call-template name="append_gndid"/>
                    <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                        <xsl:text>##xx##ZBC</xsl:text>
                    </xsl:if>
                </xsl:element>
        </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='111'] |
                         datafield[@tag='611' and matches(subfield[@code='2'], '^gnd$|^stw$')] |
                         datafield[@tag='711']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='t'])"/>
            <xsl:otherwise>
                <xsl:call-template name="browse_conference"/>
                <xsl:element name="agent">
                    <xsl:call-template name="conference_name"/>
                    <xsl:call-template name="append_gndid"/>
                    <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                        <xsl:text>##xx##ZBC</xsl:text>
                    </xsl:if>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='690']">
            <xslt:if test="matches(child::subfield[@code='2'], '^ubs-AC$')">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:call-template name="ubs-AC_names"/>
                </xsl:for-each>
            </xslt:if>
    </xsl:template>


    <xsl:template name="browse_person">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='4']/text())">
                <xsl:for-each select="child::subfield[@code='4']">
                    <xsl:variable name="relator" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$relator}">
                        <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                        <xsl:if test="preceding-sibling::subfield[@code='b'] and preceding-sibling::subfield[@code='b']/text() != ''">
                            <xsl:value-of select="concat(' ', replace(preceding-sibling::subfield[@code='b'][1], '[,.]', ''), '.')"/>
                        </xsl:if>
                        <xsl:if test="preceding-sibling::subfield[@code='c'] and preceding-sibling::subfield[@code='c']/text() != ''">
                            <xsl:value-of select="concat(', ', preceding-sibling::subfield[@code='c'][1])"/>
                        </xsl:if>
                        <xsl:if test="preceding-sibling::subfield[@code='d'] and preceding-sibling::subfield[@code='d']/text() != ''">
                            <xsl:value-of select="concat(' (', preceding-sibling::subfield[@code='d'][1], ')')"/>
                        </xsl:if>
                        <xsl:if test="matches(preceding-sibling::subfield[@code='0'], '^\(DE-588\)') or matches(following-sibling::subfield[@code='0'], '^\(DE-588\)')">
                            <xsl:value-of select="concat('##xx##', following-sibling::subfield[@code='0'], preceding-sibling::subfield[@code='0'])"/>
                        </xsl:if>
                        <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                            <xsl:text>##xx##ZBC</xsl:text>
                        </xsl:if>
                    </xsl:element>
                    <xsl:if test="$relator = 'hnr'">
                        <xsl:if test="exists(preceding::datafield[@tag='655'][matches(subfield[@code='a'], '^Nachruf$')])">
                            <xsl:element name="nekrolog">
                                <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                                <xsl:if test="preceding-sibling::subfield[@code='b'] and preceding-sibling::subfield[@code='b']/text() != ''">
                                    <xsl:value-of select="concat(' ', replace(preceding-sibling::subfield[@code='b'][1], '[,.]', ''), '.')"/>
                                </xsl:if>
                                <xsl:if test="preceding-sibling::subfield[@code='c'] and preceding-sibling::subfield[@code='c']/text() != ''">
                                    <xsl:value-of select="concat(', ', preceding-sibling::subfield[@code='c'][1])"/>
                                </xsl:if>
                                <xsl:if test="preceding-sibling::subfield[@code='d'] and preceding-sibling::subfield[@code='d']/text() != ''">
                                    <xsl:value-of select="concat(' (', preceding-sibling::subfield[@code='d'][1], ')')"/>
                                </xsl:if>
                                <xsl:if test="matches(preceding-sibling::subfield[@code='0'], '^\(DE-588\)') or matches(following-sibling::subfield[@code='0'], '^\(DE-588\)')">
                                    <xsl:value-of select="concat('##xx##', following-sibling::subfield[@code='0'], preceding-sibling::subfield[@code='0'])"/>
                                </xsl:if>
                                <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                    <xsl:text>##xx##ZBC</xsl:text>
                                </xsl:if>
                            </xsl:element>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="@tag='100'">
                        <xsl:element name="aut">
                            <xsl:call-template name="person_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='700'">
                        <xsl:element name="oth">
                            <xsl:call-template name="person_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='600'">
                        <xsl:if test="exists(following::datafield[@tag='990'][matches(subfield[@code='a'][1], '^tur', 'i')])">
                            <xsl:if test="matches(child::subfield[@code='0'], '^\(DE-588\)')">
                                <xsl:element name="turPerson">
                                    <xsl:call-template name="person_name"/>
                                    <xsl:call-template name="append_gndid"/>
                                    <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                        <xsl:text>##xx##ZBC</xsl:text>
                                    </xsl:if>
                                </xsl:element>
                            </xsl:if>
                        </xsl:if>
                        <xsl:element name="subagent">
                            <xsl:call-template name="person_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="browse_corporate">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='4']/text())">
                <xsl:for-each select="child::subfield[@code='4']">
                    <xsl:variable name="relator" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$relator}">
                        <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='b']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='b']">
                                <xsl:value-of select="concat(', ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='g']/text())">
                            <xsl:value-of select="concat(' (', preceding-sibling::subfield[@code='g'][1], ')')"/>
                        </xsl:if>
                        <xsl:if test="matches(preceding-sibling::subfield[@code='0'], '^\(DE-588\)') or matches(following-sibling::subfield[@code='0'], '^\(DE-588\)')">
                            <xsl:value-of select="concat('##xx##', following-sibling::subfield[@code='0'], preceding-sibling::subfield[@code='0'])"/>
                        </xsl:if>
                        <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                            <xsl:text>##xx##ZBC</xsl:text>
                        </xsl:if>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="@tag='110'">
                        <xsl:element name="aut">
                            <xsl:call-template name="corporate_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='710'">
                        <xsl:element name="oth">
                            <xsl:call-template name="corporate_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='610'">
                        <xsl:element name="subagent">
                            <xsl:call-template name="corporate_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="browse_conference">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='4']/text())">
                <xsl:for-each select="child::subfield[@code='4']">
                    <xsl:variable name="relator" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$relator}">
                        <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='n']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='n']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='c']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='c']">
                                <xsl:value-of select="concat(', ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='e']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='e']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='g']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='g']">
                                <xsl:value-of select="concat(' ', .)"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="exists(preceding-sibling::subfield[@code='d']/text())">
                            <xsl:for-each select="preceding-sibling::subfield[@code='d']">
                                <xsl:value-of select="concat(' (', ., ')')"/>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="matches(preceding-sibling::subfield[@code='0'], '^\(DE-588\)') or matches(following-sibling::subfield[@code='0'], '^\(DE-588\)')">
                            <xsl:value-of select="concat('##xx##', following-sibling::subfield[@code='0'], preceding-sibling::subfield[@code='0'])"/>
                        </xsl:if>
                        <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                            <xsl:text>##xx##ZBC</xsl:text>
                        </xsl:if>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="@tag='111'">
                        <xsl:element name="aut">
                            <xsl:call-template name="conference_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='711'">
                        <xsl:element name="oth">
                            <xsl:call-template name="conference_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                    <xsl:when test="@tag='611'">
                        <xsl:element name="subagent">
                            <xsl:call-template name="conference_name"/>
                            <xsl:call-template name="append_gndid"/>
                            <xsl:if test="substring(preceding::controlfield[@tag='001'], 1,3) = 'ZBC'">
                                <xsl:text>##xx##ZBC</xsl:text>
                            </xsl:if>
                        </xsl:element>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="ubs-AC_names">
        <xsl:if test=". !='Basel' and
                      . !='Bettingen' and
                      . !='Riehen' and
                      . !='Aesch' and
                      . !='Allschwil' and
                      . !='Anwil' and
                      . !='Arboldswil' and
                      . !='Arisdorf' and
                      . !='Arlesheim' and
                      . !='Augst' and
                      . !='Bennwil' and
                      . !='Biel-Benken' and
                      . !='Binningen' and
                      . !='Birsfelden' and
                      . !='Blauen' and
                      . !='Böckten' and
                      . !='Bottmingen' and
                      . !='Bretzwil' and
                      . !='Brislach' and
                      . !='Bubendorf' and
                      . !='Buckten' and
                      . !='Burg i. L.' and
                      . !='Buus' and
                      . !='Diegten' and
                      . !='Diepflingen' and
                      . !='Dittingen' and
                      . !='Duggingen' and
                      . !='Eptingen' and
                      . !='Ettingen' and
                      . !='Frenkendorf' and
                      . !='Füllinsdorf' and
                      . !='Gelterkinden' and
                      . !='Giebenach' and
                      . !='Grellingen' and
                      . !='Häfelfingen' and
                      . !='Hemmiken' and
                      . !='Hersberg' and
                      . !='Hölstein' and
                      . !='Itingen' and
                      . !='Känerkinden' and
                      . !='Itingen' and
                      . !='Känerkinden' and
                      . !='Kilchberg' and
                      . !='Lampenberg' and
                      . !='Langenbruck' and
                      . !='Laufen' and
                      . !='Läufelfingen' and
                      . !='Lausen' and
                      . !='Lauwil' and
                      . !='Liedertswil' and
                      . !='Liesberg' and
                      . !='Liestal' and
                      . !='Lupsingen' and
                      . !='Maisprach' and
                      . !='Münchenstein' and
                      . !='Muttenz' and
                      . !='Nenzlingen' and
                      . !='Niederdorf' and
                      . !='Nusshof' and
                      . !='Oberdorf' and
                      . !='Oberwil' and
                      . !='Oltingen' and
                      . !='Ormalingen' and
                      . !='Pfeffingen' and
                      . !='Pratteln' and
                      . !='Ramlinsburg' and
                      . !='Reigoldswil' and
                      . !='Reinach' and
                      . !='Rickenbach' and
                      . !='Roggenburg' and
                      . !='Röschenz' and
                      . !='Rothenfluh' and
                      . !='Rümlingen' and
                      . !='Rünenberg' and
                      . !='Schönenbuch' and
                      . !='Seltisberg' and
                      . !='Sissach' and
                      . !='Tecknau' and
                      . !='Tenniken' and
                      . !='Therwil' and
                      . !='Thürnen' and
                      . !='Titterten' and
                      . !='Wahlen' and
                      . !='Waldenburg' and
                      . !='Wenslingen' and
                      . !='Wintersingen' and
                      . !='Wittinsburg' and
                      . !='Zeglingen' and
                      . !='Ziefen' and
                      . !='Zunzgen' and
                      . !='Zwingen' and
                      . !='Rheinfelden' and
                      . !='Kaiseraugst' and
                      . !='Augusta Raurica' and
                      . !='Saint-Louis' and
                      . !='Huningue' and
                      . !='Kleinhüningen' and
                      . !='Mulhouse' and
                      . !='Mülhausen' and
                      . !='Hégenheim' and
                      . !='Hagenthal' and
                      . !='Fessenehim' and
                      . !='Colmar' and
                      . !='Eguisheim' and
                      . !='Thann' and
                      . !='Schlettstadt' and
                      . !='Séléstat' and
                      . !='Weil am Rhein' and
                      . !='Grenzach-Wyhlen' and
                      . !='Lörrach' and
                      . !='Binzen' and
                      . !='Kandern' and
                      . !='Schopfheim'">
            <xsl:element name="agent">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>