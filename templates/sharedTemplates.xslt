<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">

    <xsl:template name="person_name">
        <xsl:value-of select="child::*:subfield[@code='a']"/>
        <xsl:if test="child::*:subfield[@code='b'] and child::*:subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' ', replace(child::*:subfield[@code='b'][1], '[,.]', ''), '.')"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='c'] and child::*:subfield[@code='c']/text() != ''">
            <xsl:value-of select="concat(', ', child::*:subfield[@code='c'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='d'] and child::*:subfield[@code='d']/text() != ''">
            <xsl:value-of select="concat(' (', child::*:subfield[@code='d'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="corporate_name">
        <xsl:value-of select="child::*:subfield[@code='a']"/>
        <xsl:if test="exists(child::*:subfield[@code='b']/text())">
            <xsl:for-each select="child::*:subfield[@code='b']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::*:subfield[@code='g']/text())">
            <xsl:value-of select="concat(' (', child::*:subfield[@code='g'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="conference_name">
        <xsl:value-of select="child::*:subfield[@code='a']"/>
        <xsl:if test="exists(child::*:subfield[@code='n']/text())">
            <xsl:for-each select="child::*:subfield[@code='n']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::*:subfield[@code='c']/text())">
            <xsl:for-each select="child::*:subfield[@code='c']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::*:subfield[@code='e']/text())">
            <xsl:for-each select="child::*:subfield[@code='e']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::*:subfield[@code='g']/text())">
            <xsl:for-each select="child::*:subfield[@code='g']">
                <xsl:value-of select="concat(' ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::*:subfield[@code='d']/text())">
            <xsl:for-each select="child::*:subfield[@code='d']">
                <xsl:value-of select="concat(' (', ., ')')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <!-- use for title entries with title in $t -->
    <xsl:template name="titlePortion">
        <xsl:value-of select="child::*:subfield[@code='t']"/>
        <xsl:if test="child::*:subfield[@code='g'] and child::*:subfield[@code='g']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='g']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='m'] and child::*:subfield[@code='m']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='m']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='n'] and child::*:subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='r'] and child::*:subfield[@code='r']/text() != ''">
            <xsl:value-of select="concat(', ', child::*:subfield[@code='r'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='f'] and child::*:subfield[@code='f']/text() != ''">
            <xsl:value-of select="concat('. ', child::*:subfield[@code='f'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='s'] and child::*:subfield[@code='s']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='s']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='p'] and child::*:subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='k'] and child::*:subfield[@code='k']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='k']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='o'] and child::*:subfield[@code='o']/text() != ''">
            <xsl:value-of select="concat(' ; ', child::*:subfield[@code='o'][1])"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="namePortionPerson">
        <xsl:value-of select="child::*:subfield[@code='a']"/>
        <xsl:if test="child::*:subfield[@code='b'] and child::*:subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' ', replace(child::*:subfield[@code='b'][1], '[,.]', ''), '.')"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='c'] and child::*:subfield[@code='c']/text() != ''">
            <xsl:value-of select="concat(', ', child::*:subfield[@code='c'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='d'] and child::*:subfield[@code='d']/text() != ''">
            <xsl:value-of select="concat(' (', child::*:subfield[@code='d'][1], ')')"/>
        </xsl:if>
        <xsl:value-of select="' / '"/>
    </xsl:template>

    <!-- use for uniform title entries where title is in $a -->
    <xsl:template name="titlePortion_uniform">
        <xsl:value-of select="child::*:subfield[@code='a']"/>
        <xsl:if test="child::*:subfield[@code='t'] and child::*:subfield[@code='t']/text() != ''">
            <xsl:value-of select="concat('. ', child::*:subfield[@code='t'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='g'] and child::*:subfield[@code='g']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='g']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='m'] and child::*:subfield[@code='m']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='m']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='n'] and child::*:subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='r'] and child::*:subfield[@code='r']/text() != ''">
            <xsl:value-of select="concat(', ', child::*:subfield[@code='r'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='f'] and child::*:subfield[@code='f']/text() != ''">
            <xsl:value-of select="concat('. ', child::*:subfield[@code='f'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='s'] and child::*:subfield[@code='s']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='s']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='p'] and child::*:subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='k'] and child::*:subfield[@code='k']/text() != ''">
            <xsl:for-each select="child::*:subfield[@code='k']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='o'] and child::*:subfield[@code='o']/text() != ''">
            <xsl:value-of select="concat(' ; ', child::*:subfield[@code='o'][1])"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="placename_child">
        <xsl:value-of select="child::*:subfield[@code='a'][1]"/>
        <xsl:if test="child::*:subfield[@code='g']/text()">
            <xsl:value-of select="concat(', ', child::*:subfield[@code='g'][1])"/>
        </xsl:if>
        <xsl:if test="child::*:subfield[@code='x']/text()">
            <xsl:value-of select="concat(' (', child::*:subfield[@code='x'][1], ')')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="append_gndid">
        <xsl:if test="matches(child::*:subfield[@code='0'], '^\(DE-588\)')">
            <xsl:text>##xx##</xsl:text>
            <xsl:value-of select="child::*:subfield[@code='0']"/>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>