<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="fn">
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            todo: write something useful
        </desc>
    </doc>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>


    <xsl:template match="/">

        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="datafield[@tag='690' and matches(child::subfield[@code='2'][1], 'han-A5', 'i')]">
        <xsl:element name="han-A5">
            <xsl:value-of select="child::subfield[@code='e']"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='949']">
        <xsl:call-template name="callNumberItem"/>
    </xsl:template>

    <xsl:template name="callNumberItem">
        <xsl:variable name="library" select="child::subfield[@code='b']"/>
        <xsl:for-each select="child::subfield[matches(@code, 'h|i|j')]">
            <xsl:if test=". != '-'">
                <xsl:element name="{$library}">
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="datafield[@tag='852']">
        <xsl:call-template name="callNumberHol"/>
    </xsl:template>

    <xsl:template name="callNumberHol">
        <xsl:variable name="library" select="child::subfield[@code='b']"/>
        <xsl:for-each select="child::subfield[matches(@code, 'j|h')]">
            <xsl:if test=". != '-'">
                <xsl:element name="{$library}">
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>