<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="fn">
    <xsl:import href="sharedTemplates.xslt"/>
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <!--<xsl:import href="personTermsComponent.xslt"/>-->

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            todo: write something useful
        </desc>
    </doc>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>


    <xsl:template match="/">

        <xsl:apply-templates/>


    </xsl:template>

    <xsl:template match="datafield[@tag='130'] |
                         datafield[@tag='240'] |
                         datafield[@tag='490']">
        <xsl:element name="title">
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:call-template name="append_gndid"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='245'] |
                         datafield[@tag='246']">
        <xsl:element name="title">
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
                <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='505'] |
                         datafield[@tag='596' and @ind1='3' and @ind2='0']">
            <xsl:for-each select="child::subfield[@code='t']">
                <xsl:element name="title">
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:for-each>
    </xsl:template>

    <xsl:template match="datafield[@tag='600']">
        <xsl:if test="exists(child::subfield[@code='t'])">
            <xsl:element name="title">
                <xsl:value-of select="child::subfield[@code='t']"/>
                <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                    <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
                </xsl:if>
                <xsl:call-template name="append_gndid"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[@tag='630']">
        <xsl:element name="title">
            <xsl:value-of select="child::subfield[@code='a']"/>
            <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
            </xsl:if>
            <xsl:call-template name="append_gndid"/>
        </xsl:element>
        <xsl:element name="title">
            <xsl:value-of select="child::subfield[@code='t']"/>
            <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
                <xsl:value-of select="concat('. ', child::subfield[@code='p'][1])"/>
            </xsl:if>
            <xsl:call-template name="append_gndid"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='700']">
        <xsl:if test="exists(child::subfield[@code='t'])">
            <xsl:element name="title">
                <xsl:call-template name="titlePortion"/>
                <xsl:call-template name="append_gndid"/>
            </xsl:element>
            <xsl:element name="work">
                <xsl:call-template name="namePortionPerson"/>
                <xsl:call-template name="titlePortion"/>
                <xsl:call-template name="append_gndid"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[@tag='730']">
        <xsl:element name="title">
            <xsl:call-template name="titlePortion_uniform"/>
            <xsl:call-template name="append_gndid"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='100']">
        <xsl:if test="exists(following-sibling::datafield[@tag='240'])">
            <xsl:element name="work">
                <xsl:call-template name="namePortionPerson"/>
                <xsl:for-each select="following-sibling::datafield[@tag='240']">
                    <xsl:call-template name="titlePortion_uniform"/>
                </xsl:for-each>
            </xsl:element>
        </xsl:if>
        <xsl:element name="work">
            <xsl:call-template name="namePortionPerson"/>
            <xsl:for-each select="following-sibling::datafield[@tag='245']">
                <xsl:call-template name="titlePortion_subtitle"/>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='110']">
        <xsl:element name="work">
            <xsl:call-template name="namePortionCorporate"/>
            <xsl:for-each select="following-sibling::datafield[@tag='245']">
                <xsl:call-template name="titlePortion_subtitle"/>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="namePortionCorporate">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="exists(child::subfield[@code='b']/text())">
            <xsl:for-each select="child::subfield[@code='b']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="exists(child::subfield[@code='g']/text())">
            <xsl:value-of select="concat(' (', child::subfield[@code='g'][1], ')')"/>
        </xsl:if>
        <xsl:value-of select="' / '"/>
    </xsl:template>

    <!-- use for titles with subtitle -->
    <xsl:template name="titlePortion_subtitle">
        <xsl:value-of select="child::subfield[@code='a']"/>
        <xsl:if test="child::subfield[@code='b'] and child::subfield[@code='b']/text() != ''">
            <xsl:value-of select="concat(' : ', child::subfield[@code='b'][1])"/>
        </xsl:if>
        <xsl:if test="child::subfield[@code='n'] and child::subfield[@code='n']/text() != ''">
            <xsl:for-each select="child::subfield[@code='n']">
                <xsl:value-of select="concat('. ', .)"/>
            </xsl:for-each>
        </xsl:if>
        <xsl:if test="child::subfield[@code='p'] and child::subfield[@code='p']/text() != ''">
            <xsl:for-each select="child::subfield[@code='p']">
                <xsl:value-of select="concat(', ', .)"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>