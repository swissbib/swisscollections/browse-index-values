<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="fn">
    <xsl:import href="sharedTemplates.xslt"/>
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            todo: write something useful
        </desc>
    </doc>

    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>


    <xsl:template match="/">

        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="datafield[@tag='046']">
        <xsl:for-each select="child::subfield[matches(@code, 'b|c|d|e')]">
            <xsl:element name="date">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="datafield[@tag='264']">
        <xsl:choose>
            <xsl:when test="@ind2='0'">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:element name="production">
                        <xsl:call-template name="publishername"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="@ind2='1'">
                <xsl:for-each select="child::subfield[@code='a']">
                    <xsl:element name="production">
                        <xsl:call-template name="publishername"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="datafield[@tag='382' and matches(subfield[@code='2'], '^idsmusi|^reromusi')]">
        <xsl:call-template name="medium"/>
    </xsl:template>

    <xsl:template match="datafield[@tag='648']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$source}Time">
                        <xsl:value-of select="preceding-sibling::subfield[@code='a'][1]"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='650']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                        <xsl:element name="{$source}Topic">
                            <xsl:value-of select="preceding-sibling::subfield[@code='a'][1]"/>
                            <xsl:if test="preceding-sibling::subfield[@code='x']/text()">
                                <xsl:value-of select="concat(' (', preceding-sibling::subfield[@code='x'][1], ')')"/>
                            </xsl:if>
                            <xsl:if test="matches(preceding-sibling::subfield[@code='0'], '^\(DE-588\)') or matches(following-sibling::subfield[@code='0'], '^\(DE-588\)')">
                                <xsl:value-of select="concat('##xx##', following-sibling::subfield[@code='0'], preceding-sibling::subfield[@code='0'])"/>
                            </xsl:if>
                        </xsl:element>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='651']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$source}Geo">
                        <xsl:call-template name="placename"/>
                    </xsl:element>
                    <xsl:if test="$source='gnd'">
                        <xsl:if test="exists(following::datafield[@tag='990'][matches(subfield[@code='a'][1], '^tur', 'i')])">
                            <xsl:element name="turGeo">
                                <xsl:call-template name="placename"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="datafield[@tag='655']">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$source}Form">
                        <xsl:value-of select="preceding-sibling::subfield[@code='a'][1]"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
        <xsl:if test="matches(child::subfield[@code='2'][1], 'idsmusg')">
            <xsl:element name="idsmusg">
                <xsl:call-template name="musicGenre"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="matches(child::subfield[@code='2'][1], 'idsmusi')">
            <xsl:element name="idsadaption">
                <xsl:value-of select="child::subfield[@code='v'][1]"/>
            </xsl:element>
        </xsl:if>
        <xsl:if test="matches(child::subfield[@code='2'][1], 'gnd-content')">
            <xsl:call-template name="image"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="datafield[@tag='690'] |
                         datafield[@tag='691'] |
                         datafield[@tag='965']">
        <xsl:call-template name="local"/>
    </xsl:template>
    
    <xsl:template match="datafield[@tag='751']">
        <xsl:element name="geoPublication">
            <xsl:call-template name="placename_child"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='900' and matches(child::subfield[@code='a'][1], '^idsz1tur', 'i')]">
        <xsl:element name="tur">
            <xsl:value-of select="replace(child::subfield[@code='a'][1], 'IDSZ1', '', 'i')"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="datafield[@tag='990' and matches(child::subfield[@code='a'][1], '^tur', 'i')]">
        <xsl:element name="tur">
            <xsl:value-of select="replace(child::subfield[@code='a'][1], '_Z01', '', 'i')"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="local">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_0-9]', 'xxx')"/>
                    <xsl:element name="{$source}">
                        <xsl:choose>
                            <xsl:when test="exists(preceding-sibling::subfield[@code='e'])">
                                <xsl:value-of select="preceding-sibling::subfield[@code='e']"/>
                            </xsl:when>
                            <xsl:when test="exists(preceding-sibling::subfield[@code='a'])">
                                <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                                    <xsl:value-of select="replace(., ' \[Formschlagwort Sondersammlungen\]', '')"/>
                                </xsl:for-each>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:element>
                    <xsl:if test="$source='ubs-AC'">
                        <xsl:call-template name="ubs-AC_place"/>
                    </xsl:if>
                    <xsl:if test="$source='zbs-bib'">
                        <xsl:call-template name="zbs-bib"/>
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="medium">
        <xsl:choose>
            <xsl:when test="exists(child::subfield[@code='2']/text())">
                <xsl:for-each select="child::subfield[@code='2']">
                    <xsl:variable name="source" select="replace(., '[^A-Za-z-_]', 'xxx')"/>
                    <xsl:element name="{$source}">
                        <xsl:for-each select="preceding-sibling::subfield[@code='a']">
                            <xsl:value-of select="."/>
                            <xsl:if test="following-sibling::subfield[2][@code='v']">
                                <xsl:value-of select="concat(', ', following-sibling::subfield[2][@code='v'])" />
                            </xsl:if>
                            <xsl:if test="following-sibling::subfield[1][@code='n']">
                                <xsl:value-of select="concat(' (', following-sibling::subfield[1][@code='n'], ')')" />
                            </xsl:if>
                            <xsl:if test="exists(following-sibling::subfield[@code='a'])">
                                <xsl:value-of select="', '"/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:element>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="placename">
        <xsl:value-of select="preceding-sibling::subfield[@code='a'][1]"/>
        <xsl:if test="preceding-sibling::subfield[@code='g']/text()">
            <xsl:value-of select="concat(', ', preceding-sibling::subfield[@code='g'][1])"/>
        </xsl:if>
        <xsl:if test="preceding-sibling::subfield[@code='x']/text()">
            <xsl:value-of select="concat(' (', preceding-sibling::subfield[@code='x'][1], ')')"/>
        </xsl:if>
    </xsl:template>


    <xsl:template name="publishername">
            <xsl:if test=". != '-' and
                          . !='[S.l.]' and
                          . !='[s.l.]' and
                          . !='[S. l.]' and
                          . !='[s. l.]' and
                          . !='s. l.' and
                          . !='[S.l]' and
                          . !='[...]' and
                          . !='...' and
                          . !='[Var.loc.]' and
                          . !='[var.loc.]' and
                          . !='[Var.loc]' and
                          . !='[var.loc]' and
                          . !='[Var.loc. ]' and
                          . !='[var.loc. ]' and
                          . !='[O.O.]' and
                          . !='[o.O.]' and
                          . !='O.O.' and
                          . !='o.O.' and
                          . !='Div.' and
                          . !='div.' and
                          . !='[Div.]' and
                          . !='[div.]' and
                          . !='[Div. Erscheinungsorte]' and
                          . !='[Ohne Ort]' and
                          . !='Versch. Orte' and
                          . !='[Versch. Orte]' and
                          . !='[Erscheinungsort nicht ermittelbar]' and
                          . !='[Absendeort nicht ermittelbar]' and
                          . !='[Entstehungsort nicht ermittelbar]' and
                          . !='[Herstellungsort nicht ermittelbar]' and
                          . !='[Lieu de publication non identifié]'">
                <xsl:value-of select="replace(replace(.,'ß', 'ss', 'i'), '\[|\]|\?| etc.| \[etc.\]', '', 'i')" />
            </xsl:if>
    </xsl:template>

    <xsl:template name="musicGenre">
        <xsl:if test="child::subfield[@code='a']/text()">
            <xsl:value-of select="child::subfield[@code='a'][1]"/>
            <xsl:if test="exists(child::subfield[matches(@code, 'y|z|v')])">
                <xsl:value-of select="', '"/>
            </xsl:if>
        </xsl:if>
        <xsl:if test="child::subfield[@code='y']/text()">
            <xsl:value-of select="child::subfield[@code='y'][1]"/>
            <xsl:if test="exists(child::subfield[matches(@code, 'z|v')])">
                <xsl:value-of select="', '"/>
            </xsl:if>
        </xsl:if>
        <xsl:if test="child::subfield[@code='z']/text()">
            <xsl:value-of select="child::subfield[@code='z'][1]"/>
            <xsl:if test="exists(child::subfield[matches(@code, 'v')])">
                <xsl:value-of select="', '"/>
            </xsl:if>
        </xsl:if>
        <xsl:if test="child::subfield[@code='v']/text()">
            <xsl:value-of select="child::subfield[@code='v'][1]"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ubs-AC_place">
        <xsl:if test="preceding-sibling::subfield[@code='a'] ='Basel' or
                      preceding-sibling::subfield[@code='a'] ='Bettingen' or
                      preceding-sibling::subfield[@code='a'] ='Riehen' or
                      preceding-sibling::subfield[@code='a'] ='Aesch' or
                      preceding-sibling::subfield[@code='a'] ='Allschwil' or
                      preceding-sibling::subfield[@code='a'] ='Anwil' or
                      preceding-sibling::subfield[@code='a'] ='Arboldswil' or
                      preceding-sibling::subfield[@code='a'] ='Arisdorf' or
                      preceding-sibling::subfield[@code='a'] ='Arlesheim' or
                      preceding-sibling::subfield[@code='a'] ='Augst' or
                      preceding-sibling::subfield[@code='a'] ='Bennwil' or
                      preceding-sibling::subfield[@code='a'] ='Biel-Benken' or
                      preceding-sibling::subfield[@code='a'] ='Binningen' or
                      preceding-sibling::subfield[@code='a'] ='Birsfelden' or
                      preceding-sibling::subfield[@code='a'] ='Blauen' or
                      preceding-sibling::subfield[@code='a'] ='Böckten' or
                      preceding-sibling::subfield[@code='a'] ='Bottmingen' or
                      preceding-sibling::subfield[@code='a'] ='Bretzwil' or
                      preceding-sibling::subfield[@code='a'] ='Brislach' or
                      preceding-sibling::subfield[@code='a'] ='Bubendorf' or
                      preceding-sibling::subfield[@code='a'] ='Buckten' or
                      preceding-sibling::subfield[@code='a'] ='Burg i. L.' or
                      preceding-sibling::subfield[@code='a'] ='Buus' or
                      preceding-sibling::subfield[@code='a'] ='Diegten' or
                      preceding-sibling::subfield[@code='a'] ='Diepflingen' or
                      preceding-sibling::subfield[@code='a'] ='Dittingen' or
                      preceding-sibling::subfield[@code='a'] ='Duggingen' or
                      preceding-sibling::subfield[@code='a'] ='Eptingen' or
                      preceding-sibling::subfield[@code='a'] ='Ettingen' or
                      preceding-sibling::subfield[@code='a'] ='Frenkendorf' or
                      preceding-sibling::subfield[@code='a'] ='Füllinsdorf' or
                      preceding-sibling::subfield[@code='a'] ='Gelterkinden' or
                      preceding-sibling::subfield[@code='a'] ='Giebenach' or
                      preceding-sibling::subfield[@code='a'] ='Grellingen' or
                      preceding-sibling::subfield[@code='a'] ='Häfelfingen' or
                      preceding-sibling::subfield[@code='a'] ='Hemmiken' or
                      preceding-sibling::subfield[@code='a'] ='Hersberg' or
                      preceding-sibling::subfield[@code='a'] ='Hölstein' or
                      preceding-sibling::subfield[@code='a'] ='Itingen' or
                      preceding-sibling::subfield[@code='a'] ='Känerkinden' or
                      preceding-sibling::subfield[@code='a'] ='Itingen' or
                      preceding-sibling::subfield[@code='a'] ='Känerkinden' or
                      preceding-sibling::subfield[@code='a'] ='Kilchberg' or
                      preceding-sibling::subfield[@code='a'] ='Lampenberg' or
                      preceding-sibling::subfield[@code='a'] ='Langenbruck' or
                      preceding-sibling::subfield[@code='a'] ='Laufen' or
                      preceding-sibling::subfield[@code='a'] ='Läufelfingen' or
                      preceding-sibling::subfield[@code='a'] ='Lausen' or
                      preceding-sibling::subfield[@code='a'] ='Lauwil' or
                      preceding-sibling::subfield[@code='a'] ='Liedertswil' or
                      preceding-sibling::subfield[@code='a'] ='Liesberg' or
                      preceding-sibling::subfield[@code='a'] ='Liestal' or
                      preceding-sibling::subfield[@code='a'] ='Lupsingen' or
                      preceding-sibling::subfield[@code='a'] ='Maisprach' or
                      preceding-sibling::subfield[@code='a'] ='Münchenstein' or
                      preceding-sibling::subfield[@code='a'] ='Muttenz' or
                      preceding-sibling::subfield[@code='a'] ='Nenzlingen' or
                      preceding-sibling::subfield[@code='a'] ='Niederdorf' or
                      preceding-sibling::subfield[@code='a'] ='Nusshof' or
                      preceding-sibling::subfield[@code='a'] ='Oberdorf' or
                      preceding-sibling::subfield[@code='a'] ='Oberwil' or
                      preceding-sibling::subfield[@code='a'] ='Oltingen' or
                      preceding-sibling::subfield[@code='a'] ='Ormalingen' or
                      preceding-sibling::subfield[@code='a'] ='Pfeffingen' or
                      preceding-sibling::subfield[@code='a'] ='Pratteln' or
                      preceding-sibling::subfield[@code='a'] ='Ramlinsburg' or
                      preceding-sibling::subfield[@code='a'] ='Reigoldswil' or
                      preceding-sibling::subfield[@code='a'] ='Reinach' or
                      preceding-sibling::subfield[@code='a'] ='Rickenbach' or
                      preceding-sibling::subfield[@code='a'] ='Roggenburg' or
                      preceding-sibling::subfield[@code='a'] ='Röschenz' or
                      preceding-sibling::subfield[@code='a'] ='Rothenfluh' or
                      preceding-sibling::subfield[@code='a'] ='Rümlingen' or
                      preceding-sibling::subfield[@code='a'] ='Rünenberg' or
                      preceding-sibling::subfield[@code='a'] ='Schönenbuch' or
                      preceding-sibling::subfield[@code='a'] ='Seltisberg' or
                      preceding-sibling::subfield[@code='a'] ='Sissach' or
                      preceding-sibling::subfield[@code='a'] ='Tecknau' or
                      preceding-sibling::subfield[@code='a'] ='Tenniken' or
                      preceding-sibling::subfield[@code='a'] ='Therwil' or
                      preceding-sibling::subfield[@code='a'] ='Thürnen' or
                      preceding-sibling::subfield[@code='a'] ='Titterten' or
                      preceding-sibling::subfield[@code='a'] ='Wahlen' or
                      preceding-sibling::subfield[@code='a'] ='Waldenburg' or
                      preceding-sibling::subfield[@code='a'] ='Wenslingen' or
                      preceding-sibling::subfield[@code='a'] ='Wintersingen' or
                      preceding-sibling::subfield[@code='a'] ='Wittinsburg' or
                      preceding-sibling::subfield[@code='a'] ='Zeglingen' or
                      preceding-sibling::subfield[@code='a'] ='Ziefen' or
                      preceding-sibling::subfield[@code='a'] ='Zunzgen' or
                      preceding-sibling::subfield[@code='a'] ='Zwingen' or
                      preceding-sibling::subfield[@code='a'] ='Rheinfelden' or
                      preceding-sibling::subfield[@code='a'] ='Kaiseraugst' or
                      preceding-sibling::subfield[@code='a'] ='Augusta Raurica' or
                      preceding-sibling::subfield[@code='a'] ='Saint-Louis' or
                      preceding-sibling::subfield[@code='a'] ='Huningue' or
                      preceding-sibling::subfield[@code='a'] ='Kleinhüningen' or
                      preceding-sibling::subfield[@code='a'] ='Mulhouse' or
                      preceding-sibling::subfield[@code='a'] ='Mülhausen' or
                      preceding-sibling::subfield[@code='a'] ='Hégenheim' or
                      preceding-sibling::subfield[@code='a'] ='Hagenthal' or
                      preceding-sibling::subfield[@code='a'] ='Fessenehim' or
                      preceding-sibling::subfield[@code='a'] ='Colmar' or
                      preceding-sibling::subfield[@code='a'] ='Eguisheim' or
                      preceding-sibling::subfield[@code='a'] ='Thann' or
                      preceding-sibling::subfield[@code='a'] ='Schlettstadt' or
                      preceding-sibling::subfield[@code='a'] ='Séléstat' or
                      preceding-sibling::subfield[@code='a'] ='Weil am Rhein' or
                      preceding-sibling::subfield[@code='a'] ='Grenzach-Wyhlen' or
                      preceding-sibling::subfield[@code='a'] ='Lörrach' or
                      preceding-sibling::subfield[@code='a'] ='Binzen' or
                      preceding-sibling::subfield[@code='a'] ='Kandern' or
                      preceding-sibling::subfield[@code='a'] ='Schopfheim'">
            <xsl:element name="geoAC">
                <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template name="image">
        <xsl:if test="child::subfield[@code='a'] ='Bild' or
                      child::subfield[@code='a'] ='Bildband' or
                      child::subfield[@code='a'] ='Bilderbogen' or
                      child::subfield[@code='a'] ='Bildnis' or
                      child::subfield[@code='a'] ='Einblattdruck' or
                      child::subfield[@code='a'] ='Fotografie' or
                      child::subfield[@code='a'] ='Grafik' or
                      child::subfield[@code='a'] ='Kalender' or
                      child::subfield[@code='a'] ='Karikatur' or
                      child::subfield[@code='a'] ='Künstlerbuch' or
                      child::subfield[@code='a'] ='Modell' or
                      child::subfield[@code='a'] ='Postkarte' or
                      child::subfield[@code='a'] ='Zeichnung' ">
            <xsl:element name="imageForm">
                <xsl:value-of select="child::subfield[@code='a']"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template name="zbs-bib">
        <xsl:choose>
            <xsl:when test="matches(preceding-sibling::subfield[@code='e'], '11')">
                <xsl:element name="agentSobib">
                    <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="matches(preceding-sibling::subfield[@code='e'], '12')">
                <xsl:element name="geoSobib">
                    <xsl:value-of select="preceding-sibling::subfield[@code='a']"/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>