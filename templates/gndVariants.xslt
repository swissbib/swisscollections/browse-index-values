<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xpath-default-namespace="http://www.loc.gov/MARC21/slim"
                exclude-result-prefixes="xs"
                version="2.0">
    <xsl:import href="sharedTemplates.xslt"/>
    <!--<xsl:import href="titleTermsComponent.xslt"/>
    <xsl:import href="subjectTermsComponent.xslt"/>-->

    <xsl:output
            method="xml"
            encoding="UTF-8"
            indent="yes"
            omit-xml-declaration="yes"

    />

    <xsl:template match="/">

        <xsl:call-template name="variant_person"/>
        <xsl:call-template name="variant_corporate"/>
        <xsl:call-template name="variant_conference"/>
        <xsl:call-template name="variant_title"/>
        <xsl:call-template name="variant_subject"/>
        <xsl:call-template name="variant_place"/>


    </xsl:template>

    <xsl:template name="variant_person">
        <xsl:for-each select="/record/datafield[@tag='400']">
            <xsl:choose>
                <xsl:when test="exists(child::subfield[@code='t'])">
                    <xsl:element name="gnd">
                        <xsl:call-template name="titlePortion"/>
                    </xsl:element>
                    <xsl:element name="gndWork">
                        <xsl:call-template name="namePortionPerson"/>
                        <xsl:call-template name="titlePortion"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="gnd">
                        <xsl:call-template name="person_name"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_corporate">
        <xsl:for-each select="/record/datafield[@tag='410']">
            <xsl:element name="gnd">
                <xsl:call-template name="corporate_name"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_conference">
        <xsl:for-each select="/record/datafield[@tag='411']">
            <xsl:element name="gnd">
                <xsl:call-template name="conference_name"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_title">
        <xsl:for-each select="/record/datafield[@tag='430']">
            <xsl:element name="gnd">
                <xsl:call-template name="titlePortion_uniform"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_subject">
        <xsl:for-each select="/record/datafield[@tag='450']">
            <xsl:element name="gnd">
                <xsl:value-of select="child::subfield[@code='a'][1]"/>
                <xsl:if test="child::subfield[@code='x']/text()">
                    <xsl:value-of select="concat(' (', child::subfield[@code='x'][1], ')')"/>
                </xsl:if>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="variant_place">
        <xsl:for-each select="/record/datafield[@tag='451']">
            <xsl:element name="gnd">
                <xsl:call-template name="placename_child"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>