<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                exclude-result-prefixes="fn">
    <!--xmlns:fn="http://www.w3.org/2005/xpath-functions"> -->

    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"
    />

    <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
        <desc>
            todo: write something useful
        </desc>
    </doc>

    <xsl:template match="/">
    <browsevalues>

        <xsl:call-template name="titles">
            <xsl:with-param name="fragment" select="record" />
        </xsl:call-template>



    </browsevalues>
    </xsl:template>


    <xsl:template name="titles">
        <xsl:param name="fragment" />

        <xsl:if test="$fragment/datafield[@tag='245']/subfield[@code='a'][empty(following-sibling::subfield[@code='b'])]">
            <title><xsl:value-of select="$fragment/datafield[@tag='245']/subfield[@code='a']" /></title>
        </xsl:if>

        <xsl:if test="$fragment/datafield[@tag='245']/subfield[@code='a'][exists(following-sibling::subfield[@code='b'])]">
            <title><xsl:value-of select="concat($fragment/datafield[@tag='245']/subfield[@code='a'],$fragment/datafield[@tag='245']/subfield[@code='b'])" /></title>
        </xsl:if>

    </xsl:template>


</xsl:stylesheet>