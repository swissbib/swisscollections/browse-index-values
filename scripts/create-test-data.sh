# create topics and load data

# delete topics
docker compose exec broker bash -c "
/opt/kafka/bin/kafka-topics.sh \
  --bootstrap-server broker:29092 \
  --topic swisscollections-slsp-deduplicated \
  --delete"

docker compose exec broker bash -c "
/opt/kafka/bin/kafka-topics.sh \
  --bootstrap-server broker:29092 \
  --topic swisscollections-browse-values \
  --delete"

# create topic
docker compose exec broker bash -c "
/opt/kafka/bin/kafka-topics.sh \
  --bootstrap-server broker:29092 \
  --topic swisscollections-slsp-deduplicated \
  --replication-factor 1 \
  --partitions 4 \
  --create"

# create topic
docker compose exec broker bash -c "
/opt/kafka/bin/kafka-topics.sh \
  --bootstrap-server broker:29092 \
  --topic swisscollections-browse-values \
  --replication-factor 1 \
  --partitions 4 \
  --create"

# produce data
docker compose exec broker bash -c "
  /opt/kafka/bin/kafka-console-producer.sh \
  --bootstrap-server broker:29092 \
  --topic swisscollections-slsp-deduplicated \
  --property 'parse.key=true' \
  --property 'key.separator=|' < records.txt"
