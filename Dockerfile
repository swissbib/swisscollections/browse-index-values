FROM eclipse-temurin:21-jre-jammy

# needed to be able to include one xsl in another (sharedTemplates.xslt)
ADD templates /app/templates
WORKDIR /app

ADD target/app.jar /app/app.jar
CMD java -jar /app/app.jar

